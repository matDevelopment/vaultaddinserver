﻿using AddinServer.Models.PLM;
using AddinServer.Models.Vault;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddinServer.IRepositores
{
   public interface ICaseRepository
    {
        IEnumerable<Cases> GetAllCases();
        IEnumerable<SimplifiedCaseModel> GetAllSimplifiedCases();
    }
}
