﻿using AddinServer.Models.Vault;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddinServer.IRepositores
{
    public interface IVaultRepository
    {
        IEnumerable<View> GetViews();
        View GetViewById(int viewId);
        IEnumerable<PropertyDefinition> GetPropertyDefinitions();
        void AddBom(Bom bomData);
        IEnumerable<BomData> GetBomList(int viewId, bool highLevel, int? CaseId, DateTime[] date);
        void AddPropertyDefinition(PropertyDefinition propertyDefinition);
        void UpdatePropertyDefinition(PropertyDefinition propertyDef);
        PropertyDefinition GetPropertyByName(string name);
        object ExportToExcel(int viewId, bool highLevel, int[] idsToExport);
        object ExportToERP(int viewId, bool highLevel, int[] idsToExport);
        View AddNewView(View view);
        IEnumerable<PropertyDefinition> GetAllPropertyDefinition();
        void DeleteEarlierExport(long fileId, int caseId);
        void AddXML(List<ExcelProperty> excelProperties, string fileName);
        List<string> GetXMLnames();
        PredefinedView GetXMLvalues(string fileName);
        void SaveBom(long fileId, int caseId);
        
    }
}
