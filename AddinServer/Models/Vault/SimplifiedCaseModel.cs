﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddinServer.Models.Vault
{
    public class SimplifiedCaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
