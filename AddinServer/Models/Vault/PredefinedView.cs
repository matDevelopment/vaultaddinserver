﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddinServer.Models.Vault
{
    public class PredefinedView
    {
        public string Name { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public List<string> Properties { get; set; }
    }
}
