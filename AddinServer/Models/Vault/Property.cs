﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddinServer.Models.Vault
{
    public class Property : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set;}
        public int DefinitionId { get; set;}
        public PropertyDefinition Definition { get; set;}
        public string Value { get; set;}
        public BomData BomData { get; set; }
        public bool isAvailable { get; set; } = true;
    }
}
