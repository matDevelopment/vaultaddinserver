﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddinServer.Models.Vault
{
    public class BomData : Entity {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public long VaultItemId { get; set; }
        public string VaultItemName { get; set; }
        public Boolean WasExported { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? ExportedTime { get; set; }

        public ICollection<Property> Properties { get; set; }
        public virtual ICollection<BomData> ChildrenBom { get; set; }
        public int? CaseId { get; set; }
    }
    public class Bom 
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime ExportedTime { get; set; }
        public int BomDataId { get; set; }
        public BomData BomData { get; set; }
    }
}
