﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddinServer.Models.Vault
{
    public class BOMModel
    {
        public string Name { get; set; }
        public bool Complex { get; set; }
        public string Unit { get; set; }
        public string Description { get; set; }
        public List<BOMModel> Children { get; set; }
        public string ParentName { get; set; }
        public List<Attribute> Atributes {get;set;} 
    }
}
