﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace AddinServer.Models.Vault
{
    public class View : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<PropertyDefinition> PropertyDefinitions { get; set; }
        public string XMLName { get; set; }
    }
}

