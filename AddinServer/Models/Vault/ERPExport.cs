﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddinServer.Models.Vault
{
    public class ERPExport
    {
        public long fileId { get; set; }
        public int caseId { get; set; }
    }
}
