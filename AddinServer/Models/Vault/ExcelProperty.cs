﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddinServer.Models.Vault
{
    public class ExcelProperty
    {
        public string Name { get; set; }
        public string AddressX { get; set; }
        public string AddressY { get; set; }
    }
}
