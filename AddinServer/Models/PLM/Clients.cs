﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class Clients
    {
        public Clients()
        {
            Cases = new HashSet<Cases>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int PhoneNumber { get; set; }
        public int Nip { get; set; }

        public ICollection<Cases> Cases { get; set; }
    }
}
