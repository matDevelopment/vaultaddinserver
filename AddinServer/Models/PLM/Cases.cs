﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class Cases
    {
        public Cases()
        {
            Ecps = new HashSet<Ecps>();
            Tasks = new HashSet<Tasks>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public int? PriorityId { get; set; }
        public int? ClientId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public float TimeEstimated { get; set; }
        public float TimeTaken { get; set; }
        public int? CaseTagParentId { get; set; }
        public int? CaseTagChildId { get; set; }
        public int? GroupId { get; set; }
        public int? UserId { get; set; }

        public CaseTagChildren CaseTagChild { get; set; }
        public CaseTagParents CaseTagParent { get; set; }
        public Clients Client { get; set; }
        public Groups Group { get; set; }
        public Priorites Priority { get; set; }
        public Statuses Status { get; set; }
        public Users User { get; set; }
        public ICollection<Ecps> Ecps { get; set; }
        public ICollection<Tasks> Tasks { get; set; }
    }
}
