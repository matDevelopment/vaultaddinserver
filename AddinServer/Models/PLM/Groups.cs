﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class Groups
    {
        public Groups()
        {
            Cases = new HashSet<Cases>();
            GroupsUsers = new HashSet<GroupsUsers>();
            Memos = new HashSet<Memos>();
            Tasks = new HashSet<Tasks>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public int? TeamLeaderId { get; set; }

        public ICollection<Cases> Cases { get; set; }
        public ICollection<GroupsUsers> GroupsUsers { get; set; }
        public ICollection<Memos> Memos { get; set; }
        public ICollection<Tasks> Tasks { get; set; }
    }
}
