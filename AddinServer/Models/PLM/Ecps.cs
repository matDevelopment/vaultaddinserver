﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class Ecps
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? CaseId { get; set; }
        public int? TaskId { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public string Description { get; set; }

        public Cases Case { get; set; }
        public Tasks Task { get; set; }
        public Users User { get; set; }
    }
}
