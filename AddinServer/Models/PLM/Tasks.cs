﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class Tasks
    {
        public Tasks()
        {
            Ecps = new HashSet<Ecps>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public int? PriorityId { get; set; }
        public int? CaseId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TimeEstimated { get; set; }
        public int TimeTaken { get; set; }
        public int? TaskTagMainId { get; set; }
        public int? TaskTagAccessoryId { get; set; }
        public int? TaskTagAccessoryMinorId { get; set; }
        public int? GroupId { get; set; }
        public int? UserId { get; set; }

        public Cases Case { get; set; }
        public Groups Group { get; set; }
        public Priorites Priority { get; set; }
        public Statuses Status { get; set; }
        public TaskTagsAccessory TaskTagAccessory { get; set; }
        public TaskTagsAccessoryMinor TaskTagAccessoryMinor { get; set; }
        public TaskTagsMain TaskTagMain { get; set; }
        public Users User { get; set; }
        public ICollection<Ecps> Ecps { get; set; }
    }
}
