﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class CaseTagChildren
    {
        public CaseTagChildren()
        {
            Cases = new HashSet<Cases>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CaseTagParentId { get; set; }

        public CaseTagParents CaseTagParent { get; set; }
        public ICollection<Cases> Cases { get; set; }
    }
}
