﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class CaseTagParents
    {
        public CaseTagParents()
        {
            CaseTagChildren = new HashSet<CaseTagChildren>();
            Cases = new HashSet<Cases>();
            TaskTagsAccessory = new HashSet<TaskTagsAccessory>();
            TaskTagsAccessoryMinor = new HashSet<TaskTagsAccessoryMinor>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<CaseTagChildren> CaseTagChildren { get; set; }
        public ICollection<Cases> Cases { get; set; }
        public ICollection<TaskTagsAccessory> TaskTagsAccessory { get; set; }
        public ICollection<TaskTagsAccessoryMinor> TaskTagsAccessoryMinor { get; set; }
    }
}
