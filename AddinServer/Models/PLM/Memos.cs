﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class Memos
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int? GroupId { get; set; }
        public DateTime Time { get; set; }
        public int ReporterId { get; set; }
        public int? AssigneeId { get; set; }

        public Users Assignee { get; set; }
        public Groups Group { get; set; }
        public Users Reporter { get; set; }
    }
}
