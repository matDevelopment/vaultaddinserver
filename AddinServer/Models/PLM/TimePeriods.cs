﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class TimePeriods
    {
        public int Id { get; set; }
        public string StartTime { get; set; }
    }
}
