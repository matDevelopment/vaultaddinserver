﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class Statuses
    {
        public Statuses()
        {
            Cases = new HashSet<Cases>();
            Tasks = new HashSet<Tasks>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string BackgroundColor { get; set; }
        public string Color { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }

        public ICollection<Cases> Cases { get; set; }
        public ICollection<Tasks> Tasks { get; set; }
    }
}
