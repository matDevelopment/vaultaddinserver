﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class Users
    {
        public Users()
        {
            Cases = new HashSet<Cases>();
            Ecps = new HashSet<Ecps>();
            GroupsUsers = new HashSet<GroupsUsers>();
            MemosAssignee = new HashSet<Memos>();
            MemosReporter = new HashSet<Memos>();
            Tasks = new HashSet<Tasks>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
        public string Avatar { get; set; }
        public string Roles { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }

        public ICollection<Cases> Cases { get; set; }
        public ICollection<Ecps> Ecps { get; set; }
        public ICollection<GroupsUsers> GroupsUsers { get; set; }
        public ICollection<Memos> MemosAssignee { get; set; }
        public ICollection<Memos> MemosReporter { get; set; }
        public ICollection<Tasks> Tasks { get; set; }
    }
}
