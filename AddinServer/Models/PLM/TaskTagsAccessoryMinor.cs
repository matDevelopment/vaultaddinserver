﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class TaskTagsAccessoryMinor
    {
        public TaskTagsAccessoryMinor()
        {
            Tasks = new HashSet<Tasks>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CaseTagParentId { get; set; }

        public CaseTagParents CaseTagParent { get; set; }
        public ICollection<Tasks> Tasks { get; set; }
    }
}
