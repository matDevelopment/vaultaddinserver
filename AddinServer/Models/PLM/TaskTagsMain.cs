﻿using System;
using System.Collections.Generic;

namespace AddinServer.Models.PLM
{
    public partial class TaskTagsMain
    {
        public TaskTagsMain()
        {
            Tasks = new HashSet<Tasks>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Tasks> Tasks { get; set; }
    }
}
