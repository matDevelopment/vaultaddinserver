﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AddinServer.Migrations
{
    public partial class updatedModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BomDatas",
                columns: table => new
                {
                    IsVisible = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VaultItemId = table.Column<long>(nullable: false),
                    VaultItemName = table.Column<string>(nullable: true),
                    WasExported = table.Column<bool>(nullable: false),
                    ExportedTime = table.Column<DateTime>(type: "DateTime2", nullable: true),
                    CaseId = table.Column<int>(nullable: true),
                    BomDataId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BomDatas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BomDatas_BomDatas_BomDataId",
                        column: x => x.BomDataId,
                        principalTable: "BomDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Views",
                columns: table => new
                {
                    IsVisible = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    XMLName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Views", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Boms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExportedTime = table.Column<DateTime>(nullable: false),
                    BomDataId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Boms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Boms_BomDatas_BomDataId",
                        column: x => x.BomDataId,
                        principalTable: "BomDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PropertiesDefinitions",
                columns: table => new
                {
                    IsVisible = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SystemName = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    ViewId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertiesDefinitions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropertiesDefinitions_Views_ViewId",
                        column: x => x.ViewId,
                        principalTable: "Views",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Properties",
                columns: table => new
                {
                    IsVisible = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DefinitionId = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    BomDataId = table.Column<int>(nullable: true),
                    isAvailable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Properties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Properties_BomDatas_BomDataId",
                        column: x => x.BomDataId,
                        principalTable: "BomDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Properties_PropertiesDefinitions_DefinitionId",
                        column: x => x.DefinitionId,
                        principalTable: "PropertiesDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BomDatas_BomDataId",
                table: "BomDatas",
                column: "BomDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Boms_BomDataId",
                table: "Boms",
                column: "BomDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Properties_BomDataId",
                table: "Properties",
                column: "BomDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Properties_DefinitionId",
                table: "Properties",
                column: "DefinitionId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertiesDefinitions_ViewId",
                table: "PropertiesDefinitions",
                column: "ViewId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Boms");

            migrationBuilder.DropTable(
                name: "Properties");

            migrationBuilder.DropTable(
                name: "BomDatas");

            migrationBuilder.DropTable(
                name: "PropertiesDefinitions");

            migrationBuilder.DropTable(
                name: "Views");
        }
    }
}
