﻿using System;
using AddinServer.Models.PLM;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AddinServer.Context
{
    public partial class PLMContext : DbContext
    {
        public PLMContext()
        {
        }

        public PLMContext(DbContextOptions<PLMContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cases> Cases { get; set; }
        public virtual DbSet<CaseTagChildren> CaseTagChildren { get; set; }
        public virtual DbSet<CaseTagParents> CaseTagParents { get; set; }
        public virtual DbSet<Clients> Clients { get; set; }
        public virtual DbSet<Ecps> Ecps { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<GroupsUsers> GroupsUsers { get; set; }
        public virtual DbSet<Memos> Memos { get; set; }
        public virtual DbSet<Priorites> Priorites { get; set; }
        public virtual DbSet<Statuses> Statuses { get; set; }
        public virtual DbSet<Tasks> Tasks { get; set; }
        public virtual DbSet<TaskTagsAccessory> TaskTagsAccessory { get; set; }
        public virtual DbSet<TaskTagsAccessoryMinor> TaskTagsAccessoryMinor { get; set; }
        public virtual DbSet<TaskTagsMain> TaskTagsMain { get; set; }
        public virtual DbSet<TimePeriods> TimePeriods { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=ADNMLABUDA\\SQLEXPRESS;Database=PLM;Trusted_Connection=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cases>(entity =>
            {
                entity.HasIndex(e => e.CaseTagChildId);

                entity.HasIndex(e => e.CaseTagParentId);

                entity.HasIndex(e => e.ClientId);

                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.PriorityId);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.UserId);

                entity.HasOne(d => d.CaseTagChild)
                    .WithMany(p => p.Cases)
                    .HasForeignKey(d => d.CaseTagChildId);

                entity.HasOne(d => d.CaseTagParent)
                    .WithMany(p => p.Cases)
                    .HasForeignKey(d => d.CaseTagParentId);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Cases)
                    .HasForeignKey(d => d.ClientId);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Cases)
                    .HasForeignKey(d => d.GroupId);

                entity.HasOne(d => d.Priority)
                    .WithMany(p => p.Cases)
                    .HasForeignKey(d => d.PriorityId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Cases)
                    .HasForeignKey(d => d.StatusId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Cases)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<CaseTagChildren>(entity =>
            {
                entity.HasIndex(e => e.CaseTagParentId);

                entity.HasOne(d => d.CaseTagParent)
                    .WithMany(p => p.CaseTagChildren)
                    .HasForeignKey(d => d.CaseTagParentId);
            });

            modelBuilder.Entity<Ecps>(entity =>
            {
                entity.HasIndex(e => e.CaseId);

                entity.HasIndex(e => e.TaskId);

                entity.HasIndex(e => e.UserId);

                entity.HasOne(d => d.Case)
                    .WithMany(p => p.Ecps)
                    .HasForeignKey(d => d.CaseId);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.Ecps)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Ecps)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<GroupsUsers>(entity =>
            {
                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.UserId);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupsUsers)
                    .HasForeignKey(d => d.GroupId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.GroupsUsers)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Memos>(entity =>
            {
                entity.HasIndex(e => e.AssigneeId);

                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.ReporterId);

                entity.HasOne(d => d.Assignee)
                    .WithMany(p => p.MemosAssignee)
                    .HasForeignKey(d => d.AssigneeId);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Memos)
                    .HasForeignKey(d => d.GroupId);

                entity.HasOne(d => d.Reporter)
                    .WithMany(p => p.MemosReporter)
                    .HasForeignKey(d => d.ReporterId);
            });

            modelBuilder.Entity<Tasks>(entity =>
            {
                entity.HasIndex(e => e.CaseId);

                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.PriorityId);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.TaskTagAccessoryId);

                entity.HasIndex(e => e.TaskTagAccessoryMinorId);

                entity.HasIndex(e => e.TaskTagMainId);

                entity.HasIndex(e => e.UserId);

                entity.HasOne(d => d.Case)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.CaseId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.GroupId);

                entity.HasOne(d => d.Priority)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.PriorityId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.StatusId);

                entity.HasOne(d => d.TaskTagAccessory)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.TaskTagAccessoryId);

                entity.HasOne(d => d.TaskTagAccessoryMinor)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.TaskTagAccessoryMinorId);

                entity.HasOne(d => d.TaskTagMain)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.TaskTagMainId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<TaskTagsAccessory>(entity =>
            {
                entity.HasIndex(e => e.CaseTagParentId);

                entity.HasOne(d => d.CaseTagParent)
                    .WithMany(p => p.TaskTagsAccessory)
                    .HasForeignKey(d => d.CaseTagParentId);
            });

            modelBuilder.Entity<TaskTagsAccessoryMinor>(entity =>
            {
                entity.HasIndex(e => e.CaseTagParentId);

                entity.HasOne(d => d.CaseTagParent)
                    .WithMany(p => p.TaskTagsAccessoryMinor)
                    .HasForeignKey(d => d.CaseTagParentId);
            });
        }
    }
}
