﻿using AddinServer.Models.Vault;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddinServer.Context
{
    public class VaultContext : DbContext
    {
        public VaultContext()
        {
        }

        
        public VaultContext(DbContextOptions<VaultContext> options) : base(options) { }
        public DbSet<Bom> Boms { get; set; }
        public DbSet<BomData> BomDatas { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<PropertyDefinition> PropertiesDefinitions { get; set; }
        public DbSet<View> Views { get; set; }
    }
}
