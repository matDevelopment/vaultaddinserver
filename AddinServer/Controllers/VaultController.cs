﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AddinServer.Dtos;
using AddinServer.IRepositores;
using AddinServer.Models.Vault;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AddinServer.Controllers
{
    [Route("api/[controller]")]
    public class VaultController : Controller
    {

        private readonly IVaultRepository _vaultService;

        public VaultController(IVaultRepository vaultService)
        {
            _vaultService = vaultService;
        }

        [HttpPost("SaveBom")]
        public IActionResult SaveBom([FromBody] ERPExport exportedParameters)
        {
            try
            {
                _vaultService.SaveBom(exportedParameters.fileId, exportedParameters.caseId);
                return Ok(exportedParameters.fileId);
            }

            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("PropertyDefinitions")]
        public IActionResult GetAllPropertyDefinitions()
        {
            try
            {
                return Ok(_vaultService.GetAllPropertyDefinition());
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }

        }

        [HttpGet("PropertyDefinitions/{name}")]
        public IActionResult GetPropertyDefinitionByName(string name)
        {
            try
            {
                return Ok(_vaultService.GetPropertyByName(name));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }

        }

        [HttpPut("View")]
        public IActionResult AddNewView([FromBody] View view)
        {
            try
            {
                return Ok(_vaultService.AddNewView(view));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("List/{viewId}/{highLevel}/{CaseId}")]
        public IActionResult GetBomList(int viewId, bool highLevel, int? CaseId, [FromBody] DateTime[] date)
        {
            try
            {
                return Ok(_vaultService.GetBomList(viewId, highLevel, CaseId, date));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("View")]
        public IActionResult GetViews()
        {
            try
            {
                return Ok(_vaultService.GetViews());
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
        [HttpGet("View/{id}")]
        public IActionResult GetViewById(int id)
        {
            try
            {
                return Ok(_vaultService.GetViewById(id));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }


        [HttpPost("/api/Bom/Excel/{viewId}/{highLevel}")]
        public IActionResult ExportToExcel(int viewId, bool highLevel, [FromBody] int[] idsToExport)
        {
            try
            {
                return Ok(_vaultService.ExportToExcel(viewId, highLevel, idsToExport));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("/api/Bom/ERP/{viewId}/{highLevel}")]
        public IActionResult ExportToERP(int viewId, bool highLevel, [FromBody] int[] idsToExport)
        {
            try
            {
                return Ok(_vaultService.ExportToERP(viewId, highLevel, idsToExport));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("/api/Bom/PreDefinedViews")]
        public IActionResult GetPreDefinedViews()
        {
            return Ok(_vaultService.GetXMLnames());
        }

        [HttpPost("/api/Bom/PreDefinedViewValues")]
        public IActionResult GetPreDefinedViewValues([FromBody]PredefinedViewDto predefinedViewDto)
        {
            return Ok(_vaultService.GetXMLvalues(predefinedViewDto.FileName));
        }

        [HttpPost("/api/upload/excel")]
        public async Task<IActionResult> GetExcelFromPLMExtension(IFormFile file)
        {
            string outcomesFolderPath = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\..\..\..\DataRepository\PreDefinedExcels\";
            var filePath = Path.Combine(outcomesFolderPath, file.FileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            return Ok();
        }

        [HttpPost("/api/upload/xml")]
        public IActionResult GetXmlFromPLMExtension([FromBody]JsonModelDto jsonModelDto)
        {

            _vaultService.AddXML(jsonModelDto.ExcelProperties, jsonModelDto.FileName);
            List<PropertyDefinition> tmpList = new List<PropertyDefinition>();

            foreach (var item in jsonModelDto.ExcelProperties)
            {

                tmpList.Add(_vaultService.GetPropertyByName(item.Name));
            }
            View tmpView = new View()
            {
                Name = jsonModelDto.FileName,
                PropertyDefinitions = tmpList,
                XMLName = jsonModelDto.FileName + ".xml"


            };
            _vaultService.AddNewView(tmpView);
            return Ok();
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
