﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AddinServer.IRepositores;
using AddinServer.Models.Vault;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AddinServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaseController : Controller
    {
        private ICaseRepository _service;

        public CaseController(ICaseRepository service)
        {
            _service = service;
        }

        [HttpGet("simple")]
        public IEnumerable GetAllSimplifiedCases()
        {
            try
            {
                var simpliefiedCasesList = _service.GetAllSimplifiedCases();

                //return Ok(_service.GetAllSimplifiedCases());
                return simpliefiedCasesList;
            }
            catch (Exception exe)
            {
                ModelState.AddModelError("general error", "Error during getting all cases" + exe.Message);               
                var simpliefiedCasesList = new List<SimplifiedCaseModel>();
                //return BadRequest("Error during getting all cases" + exe.Message);
                return simpliefiedCasesList;
            }
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
