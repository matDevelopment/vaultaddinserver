﻿using AddinServer.Context;
using AddinServer.IRepositores;
using AddinServer.Models.PLM;
using AddinServer.Models.Vault;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddinServer.Repository
{
    public class CaseRepository: ICaseRepository
    {

        private PLMContext _context;

        public CaseRepository(PLMContext context)
        {
            _context = context;
        }
        public IEnumerable<Cases> GetAllCases()
       {
             return _context.Cases
                   .Include(c => c.Client)
                   .Include(t => t.Group)
                   .ToList();
                   
       }

       public IEnumerable<SimplifiedCaseModel> GetAllSimplifiedCases()
       {
            var simpliefiedCasesList = new List<SimplifiedCaseModel>();
            var test = _context.Cases.SingleOrDefault(x=> x.Id == 1);
            var CaseList = GetAllCases();
            foreach (var caseItem in CaseList)
            {
                var newSimpleCase = new SimplifiedCaseModel();
                newSimpleCase.Id = caseItem.Id;
                newSimpleCase.Name = $"{caseItem.Id}:{caseItem.Name}:{caseItem.Description}";
                simpliefiedCasesList.Add(newSimpleCase);
            }
            return simpliefiedCasesList;
        }
    }
}
