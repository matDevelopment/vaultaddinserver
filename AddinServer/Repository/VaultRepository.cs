﻿using AddinServer.Models.Vault;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Xml;
using System.Runtime.InteropServices;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Xml.Linq;
using AddinServer.Context;
using AddinServer.IRepositores;
using VaultWrapper.VaultRepository.Interfaces;
using DocumentsRepository;

namespace AddinServer.Repository
{
    public class VaultRepository : IVaultRepository
    {
        private VaultContext _context;
        private PLMContext _PLMContext;
        private readonly IDocumentsRepository _documentsRepository;

        public VaultRepository(VaultContext context, PLMContext PLMContext, IDocumentsRepository documentsRepository)
        {
            _context = context;
            _PLMContext = PLMContext;
            _documentsRepository = documentsRepository;
        }

        // public static readonly string TemplateFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\..\..\..\DataRepository\Files\";
        public static readonly string TemplateFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\DataRepository\Files\";
        public static readonly string TemplateFileName = "ExcelTemplate";
        public static readonly string OutputFilePath = Environment.CurrentDirectory + "\\..\\DataRepository\\Outcomes\\";
        public string VaultFolderStructure;
        public string FinalPath;
        List<BOMModel> list = new List<BOMModel>();


        private void UpdatePropertyDefinitions(IVaultBom bom)
        {
            var propertyDefinitions = GetPropertyDefinitions().ToArray();
            var bomHeaders = bom.GetHeaders().ToArray();

            foreach (var header in bomHeaders)
            {
                var propertyDef = propertyDefinitions.FirstOrDefault(x => x.SystemName == header.Name);

                if (propertyDef == null)
                {
                    AddPropertyDefinition(
                        new PropertyDefinition()
                        {
                            DisplayName = header.DisplayName,
                            SystemName = header.Name
                        });
                }
                else if (propertyDef.DisplayName != header.DisplayName)
                {
                    propertyDef.DisplayName = header.DisplayName;
                    UpdatePropertyDefinition(propertyDef);
                }
            }
        }

        public void SaveBom(long fileId, int caseId)
        {
           // DeleteEarlierExport(fileId, caseId);
            var bomToSave = _documentsRepository.GetBomByItemId(fileId);
            UpdatePropertyDefinitions(bomToSave);
            var bomData = ConvertToDataBom(bomToSave, null);
            bomData.CaseId = caseId;
            SaveBomData(bomData);
        }
        private BomData ConvertToDataBom(IVaultBom vaultBom, BomData parent)
        {
            var propertyDefinitions = GetPropertyDefinitions().ToArray();
            var bom = new BomData()
            {
                VaultItemId = vaultBom.Id,
                VaultItemName = vaultBom.Name,
            };
            SetUpBomProperties(vaultBom, propertyDefinitions, bom);
            SetUpChildrenBoms(vaultBom, bom);
            return bom;
        }

        private static void SetUpBomProperties(IVaultBom vaultBom, PropertyDefinition[] propertyDefinitions, BomData bom)
        {
            if (bom.Properties == null)
            {
                bom.Properties = new List<Property>();
            }
            foreach (var vaultValue in vaultBom.GetBomValues())
            {
                bom.Properties.Add(new Property()
                {
                    Definition = propertyDefinitions.First(x => String.Equals(x.SystemName, vaultValue.Name)),
                    Value = vaultValue.Value
                });
            }
        }

        private void SetUpChildrenBoms(IVaultBom vaultBom, BomData bom)
        {
            if (bom.ChildrenBom == null)
            {
                bom.ChildrenBom = new List<BomData>();
            }
            foreach (var child in vaultBom.GetChildrenBoms())
            {
                bom.ChildrenBom.Add(ConvertToDataBom(child, bom));
            }
        }

        private void SaveBomData(BomData bomData)
        {
            var now = DateTime.Now;
            var bom = new Bom()
            {
                ExportedTime = now,
                BomData = bomData,

            };
            AddBom(bom);
        }

        public IEnumerable<Bom> GetAllBoms()
        {
            return _context.Boms;
        }

        public Bom GetBomById(int id)
        {
            return _context.Boms.Single(bom => bom.Id == id);
        }

        public IEnumerable<PropertyDefinition> GetPropertyDefinitions()
        {
            return _context.PropertiesDefinitions;
        }

        public void AddBom(Bom bom)
        {
            try
            {
                AddBomData(bom.BomData);

                _context.Boms.Add(bom);
                _context.SaveChanges();

            }
            catch (Exception exe)
            {
                throw exe;
            }

        }
        private void AddBomData(BomData data)
        {
            data.WasExported = false;
            data.ExportedTime = new DateTime();

            if (data.ChildrenBom != null)
            {
                foreach (var child in data.ChildrenBom)
                {
                    child.CaseId = data.CaseId;
                    AddBomData(child);
                }

            }
            _context.Properties.AddRange(data.Properties);
            _context.SaveChanges();
            _context.BomDatas.Add(data);
            _context.SaveChanges();


        }

        public void AddPropertyDefinition(PropertyDefinition propertyDefinition)
        {
            _context.PropertiesDefinitions.Add(propertyDefinition);
            _context.SaveChanges();
        }

        public void UpdatePropertyDefinition(PropertyDefinition propertyDef)
        {
            var propertyDefFromDB = _context.PropertiesDefinitions.First(x => x.Id == propertyDef.Id);
            propertyDefFromDB.CloneFrom(propertyDef);
            this._context.Entry(propertyDefFromDB).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public IEnumerable<PropertyDefinition> GetAllPropertyDefinition()
        {
            return _context.PropertiesDefinitions;
        }

        public View AddNewView(View view)
        {
            List<int> order = new List<int>();
            //view.PropertyDefinitions.ToList().ForEach(x => order.Add(x.Id));
            //view.Data = order.ToArray();
            _context.Views.Add(view);
            //view.PropertyDefinitions.ToList().ForEach(x => context.PropertiesDefinitions.Attach(x));
            _context.SaveChanges();
            return view;
        }


        private IEnumerable<BomData> GetAllBomDatas()
        {
            List<BomData> tempListBomData2 = new List<BomData>();
            List<BomData> tempListBomData = _context.BomDatas
                 .Include(x => x.Properties)
                 .Include(x => x.Properties.Select(y => y.Definition))
                 .Include(x => x.ChildrenBom)
                 .ToList();

            foreach (var item in tempListBomData)
            {
                tempListBomData2.Add(item);
            }

            return tempListBomData2;
        }

        private IEnumerable<BomData> GetPeriodBomDatas(DateTime[] date)
        {
            var startDate = date[0];
            var endDate = date[1];
            IEnumerable<Bom> boms = _context.Boms.Where(x => x.ExportedTime >= startDate && x.ExportedTime <= endDate)
               .Include(x => x.BomData);
            List<BomData> bomDatas = new List<BomData>();

            boms.ToList().ForEach(x => GetBom(x.BomData, bomDatas));
            return bomDatas;
        }

        private void GetBom(BomData bomData, List<BomData> bomDatas)
        {
            var Data = _context.BomDatas
                  .Where(x => x.Id == bomData.Id)
                  .Include(x => x.Properties)
                  .Include(x => x.ChildrenBom)
                  .Single();
            bomDatas.Add(Data);
            bomData.ChildrenBom.ToList().ForEach(x => GetBom(x, bomDatas));
        }
        public List<Property> GetPropertyList()
        {
            return _context.Properties.ToList();
        }

        public PropertyDefinition GetPropertyByName(string name)
        {
            return _context.PropertiesDefinitions.Single(x => x.DisplayName == name);
        }

        private void FixPropertyList()
        {
            _context.Properties.ToList().ForEach(
                x =>
                {
                    x.isAvailable = true;

                });
            _context.SaveChanges();
        }
        public List<ExcelProperty> GetPropertyListForView(int viewId)
        {
            string XMLName = _context.Views.Where(x => x.IsVisible)
                .Where(x => x.Id == viewId).Single().XMLName;
            XmlNodeList nodeList = GetNodeListForFile(XMLName);

            List<ExcelProperty> returnList = new List<ExcelProperty>();


            foreach (XmlNode propertyNode in nodeList)
            {
                ExcelProperty tmpExcelProp = new ExcelProperty();
                var childNodes = propertyNode.ChildNodes;
                tmpExcelProp.Name = childNodes[0].InnerText;
                tmpExcelProp.AddressX = childNodes[1].InnerText;
                tmpExcelProp.AddressY = childNodes[2].InnerText;
                returnList.Add(tmpExcelProp);

            }
            return returnList;

        }

        public List<int> GetIdlistForPropertyDefinitionName(List<string> nameList)
        {
            List<int> returnList = new List<int>();
            foreach (var name in nameList)
            {
                returnList.Add(_context.PropertiesDefinitions.Where(x => x.DisplayName == name).Single().Id);
            }

            return returnList;
        }

        public View GetViewById(int viewId)
        {
            return _context.Views.Single(x => x.Id == viewId);
        }

        public IEnumerable<BomData> GetBomList(int viewId, bool highLevel, int? CaseId, DateTime[] date)
        {

            View view = _context.Views.Where(x => x.Id == viewId).SingleOrDefault();
            IEnumerable<BomData> bomDatas;
            List<int> tmpPropIdList = new List<int>();
            List<ExcelProperty> list = GetPropertyListForView(viewId);




            if (date.Length > 0)
            {
                bomDatas = GetPeriodBomDatas(date);
            }
            else
            {
                bomDatas = GetAllBomDatas();
            }

            if (CaseId != null)
            {
                bomDatas = bomDatas.Where(x => x.CaseId == CaseId);
            }

            int parentId = 0;
            if (highLevel)
            {
                IList<BomData> bomDataCopy = new List<BomData>();
                bomDatas.ToList().ForEach(x => x.Properties.ToList().ForEach(
                     y =>
                     {
                         if (y.Definition.DisplayName.Equals("Parent") && y.Value == null)
                         {
                             parentId = y.DefinitionId;
                             bomDataCopy.Add(x);
                         }
                     }));

                bomDatas = bomDataCopy;
            }



            int stateID = 0;

            bomDatas.ToList().ForEach(
                x => x.Properties.ToList().ForEach(
                y =>
                {
                    if (y.Definition.DisplayName.Equals("State"))
                        stateID = y.DefinitionId;

                    if (!list.Exists(z => z.Name == y.Definition.DisplayName) && !y.Definition.DisplayName.Equals("State") || (highLevel && y.Definition.DisplayName.Equals("Parent") && !y.Definition.DisplayName.Equals("State")))
                    {
                        // x.Properties.Remove(y); ////NULLER 
                        x.Properties.Single(z => z.Id == y.Id).isAvailable = false;
                    }

                }));





            //int[] order = view.Data;
            List<string> propertyOrder = new List<string>();
            List<int> propertyIdOrder = new List<int>();

            foreach (var property in list)
            {
                propertyOrder.Add(property.Name);
            }

            propertyIdOrder = GetIdlistForPropertyDefinitionName(propertyOrder);
            int[] order = propertyIdOrder.ToArray();


            //if (stateID != 0 && !order.Contains(stateID)) {
            //    Array.Resize(ref order, order.Length + 1);
            //    order[order.Length - 1] = stateID;
            //}

            bomDatas.ToList().ForEach(
              x =>
              {

                  foreach (var prop in x.Properties)
                  {
                      tmpPropIdList.Add(prop.DefinitionId);
                  }
                  List<Property> correctOrderPropertyList = new List<Property>();
                  for (int i = 0; i < order.Length; i++)
                  {
                      if (highLevel && order[i] == parentId)
                          continue;
                      if (!tmpPropIdList.Contains(order[i]))
                      {
                          Property tmpProperty = new Property();
                          tmpProperty.DefinitionId = order[i];
                          var definition = _context.PropertiesDefinitions.Single(z => z.Id == tmpProperty.DefinitionId);
                          tmpProperty.Value = null;
                          tmpProperty.Definition = definition;
                          tmpProperty.isAvailable = true;
                          correctOrderPropertyList.Add(tmpProperty);

                      }
                      else
                      {
                          Property tmpProperty2 = new Property();
                          tmpProperty2 = x.Properties.Single(y => y.DefinitionId == order[i]);
                          tmpProperty2.isAvailable = true;
                          correctOrderPropertyList.Add(tmpProperty2);
                      }
                  }

                  x.Properties.ToList().ForEach(
                        y =>
                        {

                            if (!order.Contains(y.DefinitionId))
                            {
                                var tmpProp = new Property();
                                tmpProp = y;
                                tmpProp.isAvailable = false;
                                correctOrderPropertyList.Add(tmpProp);
                            }



                        });
                  tmpPropIdList.Clear();
                  x.Properties = correctOrderPropertyList; ////NULLER

              });




            return bomDatas;
        }

        public IEnumerable<View> GetViews()
        {
            return _context.Views;
        }
        public PropertyDefinition GetPropertyDefinitionById(int id)
        {
            return _context.PropertiesDefinitions.
                Where(x => x.IsVisible == true).
                Where(x => x.Id == id).
                Single();
        }

        public string GetFolderNameByPropertyId(int id)
        {
            return _PLMContext.Cases.FirstOrDefault(
                z => z.Id == _context.BomDatas.FirstOrDefault(
                    y => y.Id == id).CaseId).Name;
        }

        public void SetERPItem(BomData data, string parentName)
        {
            BOMModel tmpItem = new BOMModel();
            tmpItem.Unit = "szt";
            tmpItem.Name = data.VaultItemName;
            tmpItem.Description = data.Properties.Where(y => y.Definition.SystemName == "Description(Item,CO)").Single().Value;
            tmpItem.Atributes = new List<Models.Vault.Attribute>();
            foreach (var prop in data.Properties)
            {

                tmpItem.Atributes.Add(new Models.Vault.Attribute { Name = prop.Definition.DisplayName, Value = prop.Value });

            }
            tmpItem.Complex = true;
            tmpItem.ParentName = parentName;
            if (!(data.ChildrenBom.Count == 0))
            {

                tmpItem.Children = new List<BOMModel>();
                foreach (var child in data.ChildrenBom)
                {

                    SetERPItem(child, tmpItem.Name);


                }
                list.Add(tmpItem);
            }
            else
            {
                tmpItem.Complex = false;
                list.Add(tmpItem);
            }



        }
        public string GetVaultFolderPath(long folderId)
        {
            IFolderAdapter Folder = _documentsRepository.GetFolder(folderId, false);
            return Folder.FullName;
        }
        public long GetCaseFolderId()
        {
            var configUri = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\..\..\..\DocumentsRepository\Config\{Environment.MachineName}-ServerConfig.xml";
            //var configUri = $@"C:\DocumentsRepository\Config\{Environment.MachineName}-ServerConfig.xml";
            string xmlText = File.ReadAllText(configUri);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlText);
            XmlNode caseNode = doc.DocumentElement.SelectSingleNode("CasesRootFolderId");
            long id;
            long.TryParse(caseNode.InnerText.ToString(), out id);
            return id;
        }

        public object ExportToERP(int viewId, bool highLevel, int[] idsToExport)
        {

            string folderPath = GetVaultFolderPath(GetCaseFolderId());
            DateTime[] dataTime = new DateTime[] { };

            IList<BomData> bomList = GetBomList(viewId, false, null, dataTime).ToList();
            List<BomData> bomListToExport = new List<BomData>();
            View viewToExport = GetViewById(viewId);

            bomList.ToList().ForEach(x =>
            {
                if (idsToExport.Contains(x.Id))
                {

                    bomListToExport.Add(x);
                    x.WasExported = true;
                    x.ExportedTime = DateTime.Now;
                    // AddAllChildrens(x, bomListToExport);

                }
            });

            _context.SaveChanges();
            bomList = bomListToExport;
            FixPropertyList();




            foreach (var item in bomList)
            {
                SetERPItem(item, null);
            }

            foreach (var item in list)
            {
                if (!(item.ParentName == null))
                {
                    list.Where(x => x.Name == item.ParentName).Single().Children.Add(item);
                }
            }

            PostBoms(list);


            return true;

        }



        static void PostBoms(List<BOMModel> lista)
        {
            using (var client = new WebClient())
            {

                var jsonProperties = JsonConvert.SerializeObject(new JsonModel { Boms = lista });
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.Encoding = Encoding.UTF8;
                string result = client.UploadString("http://localhost:7702/api/BOM/Create", jsonProperties);


            }
        }


        public object ExportToExcel(int viewId, bool highLevel, int[] idsToExport)
        {
            string folderPath = GetVaultFolderPath(GetCaseFolderId());

            Excel.Application xlApp = new Excel.Application();

            DateTime[] dataTime = new DateTime[] { };
            if (xlApp == null)
            {
                return new Exception();
            }
            //Prepare Copy
            string orginalPath = $"{TemplateFilePath}{TemplateFileName}.xlsx";
            //string copyPath = $"{TemplateFilePath}{TemplateFileName}copy.xlsx";
            //Excel.Workbook wbook = xlApp.Workbooks.Open(orginalPath, 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", true, false, 0, true, 1, 0);
            //wbook.SaveAs($"{TemplateFileName}copy.xlsx", Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
            //       false, false, Excel.XlSaveAsAccessMode.xlNoChange,
            //       Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            //wbook.Save();
            //Kill(xlApp);

            //FileInfo fileInfo = new FileInfo(orginalPath);
            //while (IsFileLocked(fileInfo))
            //{
            //}

            Excel.Application xlApp2 = new Excel.Application();
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            //xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkBook = xlApp2.Workbooks.Open(orginalPath, 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", true, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            IList<BomData> bomList = GetBomList(viewId, false, null, dataTime).ToList();
            List<BomData> bomListToExport = new List<BomData>();
            View viewToExport = GetViewById(viewId);
            XmlNodeList NodeList = GetNodeListForFile(viewToExport.Name + ".xml");
            ExcelProperty tmpProperty = new ExcelProperty();

            //Znajdż Order Number po poroperty id
            string FolderName = GetFolderNameByPropertyId(idsToExport[0]);

            var tempPropList = GetPropertyList();
            var fixList = new List<Property>();

            bomList.ToList().ForEach(x =>
            {
                if (idsToExport.Contains(x.Id))
                {

                    bomListToExport.Add(x);
                    x.WasExported = true;
                    x.ExportedTime = DateTime.Now;
                    AddAllChildrens(x, bomListToExport);
                }
            });

            _context.SaveChanges();
            bomList = bomListToExport;
            //bomList[0].Properties.ToList().ForEach(x =>
            //{
            //    xlWorkSheet.Cells[1, bomList[0].Properties.ToList().IndexOf(x) + 1] = x.Definition.DisplayName;
            //}); ///NAGŁÓWKI

            int rows = 0;


            bomList.ToList().ForEach(x =>
            {


                x.Properties.ToList().ForEach(y =>
                {
                    if (y.isAvailable == true)
                    {
                        tmpProperty = GetXMLvaluesForProperty(NodeList, GetPropertyDefinitionById(y.DefinitionId).DisplayName);
                        string tmpAddress = tmpProperty.AddressX + tmpProperty.AddressY;
                        // xlWorkSheet.Cells[21, 5] = y.Value;
                        xlWorkSheet.Cells[Int32.Parse(tmpProperty.AddressY) + rows, Int32.Parse(tmpProperty.AddressX)] = y.Value;

                    }
                });
                rows++;
            });

            VaultFolderStructure = $"{folderPath}/{FolderName}";

            DateTime time = DateTime.Now;
            FinalPath = $"{OutputFilePath}{time.ToString("yyyyMMddHHmmssfff")}.xls";


            xlWorkSheet.Columns.AutoFit();
            xlWorkSheet.Rows.AutoFit();
            //xlWorkBook.SaveAs(time.ToString("yyyyMMddHHmmssfff") + ".xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            //xlWorkBook.SaveAs(FinalPath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.SaveAs(FinalPath, Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                    false, false, Excel.XlSaveAsAccessMode.xlNoChange,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp2.Quit();

            Marshal.FinalReleaseComObject(xlWorkSheet);
            Marshal.FinalReleaseComObject(xlWorkBook);
            Marshal.FinalReleaseComObject(xlApp2);

            xlWorkSheet = null;
            xlWorkBook = null;
            xlApp2 = null;

            //if (File.Exists(copyPath))
            //{
            //    File.Delete(copyPath);


            //}

            ErpExport.ErpExportCommandExtension export = new ErpExport.ErpExportCommandExtension();
            export.AddExcelToVault(VaultFolderStructure, FinalPath);

            FixPropertyList();

            return true;
        }
        public void AddAllChildrens(BomData data, List<BomData> listToExport)
        {
            if (data.ChildrenBom != null)
            {
                foreach (var child in data.ChildrenBom)
                {
                    listToExport.Add(child);
                    AddAllChildrens(child, listToExport);
                }
            }
        }
        public void DeleteEarlierExport(long fileId, int caseId)
        {
            IEnumerable<BomData> bomDatas = _context.BomDatas
            .Include("Properties")
            .Include(x => x.Properties.Select(y => y.Definition))
            .Include(x => x.ChildrenBom)
            .Where(x => x.VaultItemId == fileId);

            IEnumerable<BomData> bomDatas2 = _context.BomDatas
                .Include("Properies")
                .Include(x => x.Properties.Select(y => y.Definition))
                .Include(x => x.ChildrenBom);


            List<BomData> bomDatasToDelete = new List<BomData>();
            List<Bom> bomsToDelete = new List<Bom>();
            if (bomDatas.Count() != 0)
            {
                bomDatas.ToList().ForEach(x => {

                    Bom bom = _context.Boms.Where(y => y.BomDataId == x.Id).SingleOrDefault();
                    BomData bomData = _context.BomDatas.Where(z => z.Id == x.Id).SingleOrDefault();
                    if (bom != null && bomData.CaseId == caseId)
                    {
                        if (x.ChildrenBom != null)
                            DeleteChildren(x.ChildrenBom);
                        bomsToDelete.Add(bom);
                        bomDatasToDelete.Add(x);
                    }

                });
            }
           
            _context.Boms.RemoveRange(bomsToDelete);
            _context.BomDatas.RemoveRange(bomDatasToDelete);
            _context.SaveChanges();
        }

        private void DeleteChildren(ICollection<BomData> childrenBom)
        {
            childrenBom.ToList().ForEach(x => {
                if (x.ChildrenBom != null)
                    DeleteChildren(x.ChildrenBom);
                x = _context.BomDatas
                .Where(y => y.Id == x.Id)
                .Include(y => y.Properties)
                .Include(y => y.ChildrenBom)
                .Single();
                _context.Entry(x).State = EntityState.Deleted;
                x.Properties.ToList().ForEach(y => _context.Entry(y).State = EntityState.Deleted);
                _context.SaveChanges();
            });
        }
        [DllImport("User32.dll")]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int ProcessId);
        public static void Kill(Excel.Application theApp)
        {
            int id = 0;
            IntPtr intptr = new IntPtr(theApp.Hwnd);
            System.Diagnostics.Process p = null;
            try
            {
                GetWindowThreadProcessId(intptr, out id);
                p = System.Diagnostics.Process.GetProcessById(id);
                if (p != null)
                {
                    p.Kill();
                    p.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            Marshal.FinalReleaseComObject(theApp);
        }
        private static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        public List<string> GetXMLnames()
        {
            string preDefinedViewsDirectory = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\..\..\..\DataRepository\PreDefinedViews";
            DirectoryInfo d = new DirectoryInfo(preDefinedViewsDirectory);
            FileInfo[] files = d.GetFiles("*.xml"); //Getting XML files
            List<string> preDefinedList = new List<string>();
            foreach (FileInfo file in files)
            {
                preDefinedList.Add(file.Name);
            }
            return preDefinedList;
        }
        public XmlNodeList GetNodeListForFile(string fileName)
        {
            string preDefinedViewFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\..\..\..\DataRepository\PreDefinedViews\" + fileName;
            string xmlText = File.ReadAllText(preDefinedViewFilePath);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlText);

            XmlNodeList propertyNodes = doc.DocumentElement.SelectNodes("/properties/property");

            return propertyNodes;
        }

        public PredefinedView GetXMLvalues(string fileName)
        {
            string preDefinedViewFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\..\..\..\DataRepository\PreDefinedViews\" + fileName;
            string xmlText = File.ReadAllText(preDefinedViewFilePath);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlText);

            XmlNodeList propertyNodes = doc.DocumentElement.SelectNodes("/properties/property");

            List<string> preDefinedValuesList = new List<string>();
            List<ExcelProperty> properties = new List<ExcelProperty>();
            foreach (XmlNode propertyNode in propertyNodes)
            {
                var childNodes = propertyNode.ChildNodes;
                var child = childNodes[0].InnerText;
                Console.WriteLine(propertyNode.InnerText);
                preDefinedValuesList.Add(childNodes[0].InnerText);
            }
            var predefinedView = new PredefinedView
            {

                Properties = preDefinedValuesList
            };
            return predefinedView;
        }
        public ExcelProperty GetXMLvaluesForProperty(XmlNodeList propertyNodes, string propertyName)
        {
            ExcelProperty returnProperty = new ExcelProperty();
            foreach (XmlNode propertyNode in propertyNodes)
            {
                var childNodes = propertyNode.ChildNodes;
                if (childNodes[0].InnerText == propertyName)
                {
                    returnProperty.Name = childNodes[0].InnerText;
                    returnProperty.AddressX = childNodes[1].InnerText;
                    returnProperty.AddressY = childNodes[2].InnerText;
                    break;
                }

            }

            return returnProperty;
        }
        public void AddXML(List<ExcelProperty> excelProperties, string fileName)
        {
            string preDefinedViewFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\..\..\..\DataRepository\PreDefinedViews\" + fileName + ".xml";
            XDocument doc = new XDocument();
            var propertiesNode = new XElement("properties");
            foreach (var excelProperty in excelProperties)
            {
                var propertyNode = new XElement("property");
                propertyNode.Add(new XElement("name", excelProperty.Name));
                propertyNode.Add(new XElement("x", excelProperty.AddressX));
                propertyNode.Add(new XElement("y", excelProperty.AddressY));
                propertiesNode.Add(propertyNode);
            }
            doc.Add(propertiesNode);
            doc.Save(preDefinedViewFilePath);
        }
    }
}
