﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddinServer.Dtos
{
    public class PredefinedViewDto
    {
        public string FileName { get; set; }
    }
}
