﻿using AddinServer.Models.Vault;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddinServer.Dtos
{
    public class JsonModelDto
    {
        public List<ExcelProperty> ExcelProperties { get; set; }
        public string FileName { get; set; }
    }
}
