﻿using PLMVaultExtensionCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Autodesk.Connectivity.WebServices;
using System.Linq;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using PLMVaultExtensionCore.VaultRepository.Adapters;

namespace PLMVaultExtensionCore.Implementation
{
    public class VaultPropertySetter : IPropertySetter
    {

        public void SetProperty(IEntityData[] entitiesData, IConnectionAdapter conn)
        {
            IItemServiceAdapter itemService = conn.ItemService;
            IPropertyServiceAdapter propertyService = conn.PropertyService;
            //PropDef[] prtDefs = conn.WebServiceManager.PropertyService.GetPropertyDefinitionsByEntityClassId("ITEM");
            IPropDefAdapter[] prtDefs = propertyService.GetPropertyDefinitionsByEntityClassId("ITEM");

            foreach (var entityData in entitiesData)
            {
                var item = GetItem(itemService, entityData);
                var editItem = GetEditItem(itemService, item);

                var propInstParamArray = GetPropInstParamArray(prtDefs, entityData);

                UpdateItem(itemService, editItem, propInstParamArray);

            }
        }

        private static void UpdateItem(IItemServiceAdapter service, IItemAdapter editItem, IPropInstParamArrayAdapter[] propInstParamArray)
        {
            service.UpdateItemProperties(new[] { editItem.RevId }, propInstParamArray);
            service.UpdateAndCommitItems(new[] { editItem });
        }

        private static IPropInstParamArrayAdapter[] GetPropInstParamArray(IPropDefAdapter[] prtDefs, IEntityData entityData)
        {
            try
            {
                PropInstParamArray propInstanceParamArray2 = new PropInstParamArray()
                {
                    Items = ConvertToPropInstParams(entityData, prtDefs).Select(x => x.Adaptee).ToArray()
                };
            }catch(Exception exe)
            {

            }
            PropInstParamArray propInstanceParamArray = new PropInstParamArray()
            {
                Items = ConvertToPropInstParams(entityData, prtDefs).Select(x => x.Adaptee).ToArray()
            };

            return new PropInstParamArrayAdapter[] {
                new PropInstParamArrayAdapter( propInstanceParamArray)
            };
        }
    

    private static IItemAdapter GetEditItem(IItemServiceAdapter itemService, IItemAdapter item)
    {
        return itemService.EditItems(item);
    }

    private static IEnumerable<PropInstParamAdapter> ConvertToPropInstParams(IEntityData instanceParameter, IPropDefAdapter[] prtDefs)
    {
        return instanceParameter.Properties.Select(prop =>
        {
            return new PropInstParamAdapter(new PropInstParam()
            {
                         PropDefId = prtDefs.FirstOrDefault(propDef => String.Equals(propDef.DispName, prop.Key)).Id,
                Val = prop.Value
            });

        });
    }

    private static IItemAdapter GetItem(IItemServiceAdapter itemService, IEntityData instanceParameter)
    {
        return itemService.GetItemsById(new long[] { instanceParameter.Id });
    }

   }
}
