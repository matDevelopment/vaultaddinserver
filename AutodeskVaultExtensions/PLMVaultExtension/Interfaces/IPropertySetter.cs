﻿using System;
using System.Collections.Generic;
using System.Text;
using VDF = Autodesk.DataManagement.Client.Framework;

namespace PLMVaultExtensionCore.Interfaces
{
    public interface IPropertySetter
    {
        void SetProperty(IEntityData[] properties,IConnectionAdapter conn);
    }
}
