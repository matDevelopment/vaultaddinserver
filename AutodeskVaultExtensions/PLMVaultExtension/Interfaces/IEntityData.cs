﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore
{
    public interface IEntityData
    {
        string Type { get; set; }
        long Id { get; set; }
        Dictionary<string,string> Properties { get; set; }
    }
}
