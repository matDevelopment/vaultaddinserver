﻿using PLMVaultExtensionCore.VaultRepository.Adapters;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.Interfaces
{
    public interface IItemServiceAdapter
    {

        void UpdateItemProperties(long[] itemsRevIds, IPropInstParamArrayAdapter[] propInstParamArray);
        void UpdateAndCommitItems(IItemAdapter[] itemsRevisions);
        IItemAdapter EditItems(IItemAdapter item);
        IItemAdapter GetItemsById(long[] itemsIds);
    }
}
