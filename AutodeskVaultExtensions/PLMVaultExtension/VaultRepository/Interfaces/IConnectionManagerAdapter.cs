﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Interfaces
{
    public interface IConnectionManagerAdapter
    {
        ILogInResultAdapter LogIn(string serverName, string vaultName, string userName, string password);
        bool LogOut();
    }
}
