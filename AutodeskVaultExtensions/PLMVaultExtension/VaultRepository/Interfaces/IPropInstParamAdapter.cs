﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Interfaces
{
   public interface IPropInstParamAdapter
    {
        long PropDefId { get; }
        object Val { get; }
    }
}
