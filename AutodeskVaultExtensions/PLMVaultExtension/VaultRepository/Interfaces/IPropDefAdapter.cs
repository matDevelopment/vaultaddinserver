﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Interfaces
{
    public interface IPropDefAdapter
    {
        String DispName { get; }
        long Id { get; }
    }
}
