﻿using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Autodesk.DataManagement.Client.Framework.Vault.Services;
using VDF = Autodesk.DataManagement.Client.Framework;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVaultExtensionCore.VaultRepository.Adapters
{
    public class ConnectionManagerAdapter : IConnectionManagerAdapter
    {
        private IVaultConnectionManagerService _adaptee = VDF.Vault.Library.ConnectionManager;
        private string _serverName;
        private string _vaultName;
        private string _userName;
        private string _password;
        private AuthenticationFlags _authType;

        public void CloseAllConnections()
        {

            _adaptee.CloseAllConnections();
        }

        public ILogInResultAdapter LogIn(string serverName, string vaultName, string userName, string password)
        {
            try
            {
                //As per https://forums.autodesk.com/t5/vault-forum/vault-2018-web-app-login-issue/td-p/7273764
                //WebServiceManager.AllowUI = false;

                this._serverName = serverName;
                this._vaultName = vaultName;
                this._userName = userName;
                this._password = password;
                this._authType = VDF.Vault.Currency.Connections.AuthenticationFlags.Standard;
                return new LogInResultAdapter(_adaptee.LogIn(serverName, vaultName, userName, password, _authType, null));
            }
            catch (Exception exe)
            {
                throw;
            }
        }

        public bool LogOut()
        {
            return _adaptee.LogOut(VDF.Vault.Library.ConnectionManager.GetExistingConnection(_serverName, _vaultName, _userName, _password, _authType));
        }

    }
}
