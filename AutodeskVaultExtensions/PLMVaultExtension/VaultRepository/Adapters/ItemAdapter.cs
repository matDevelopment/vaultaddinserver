﻿using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Autodesk.Connectivity.WebServices;

namespace PLMVaultExtensionCore.VaultRepository.Adapters
{
   public class ItemAdapter : IItemAdapter
    {
        

        public ItemAdapter(Item item)
        {
            this._adaptee = item;
        }

        public Item _adaptee { get; }

        public long RevId
        { get
            {
                return _adaptee.RevId;
            }
        }
    }
}
