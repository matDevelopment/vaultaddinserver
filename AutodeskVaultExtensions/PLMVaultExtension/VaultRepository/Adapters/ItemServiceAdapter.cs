﻿using PLMVaultExtensionCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using Autodesk.Connectivity.WebServices;
using System.Linq;

namespace PLMVaultExtensionCore.VaultRepository.Adapters
{
    class ItemServiceAdapter : IItemServiceAdapter
    {
        ItemService _adaptee;

       public ItemServiceAdapter(ItemService itemService)
        {
            _adaptee = itemService;
        }

        public IItemAdapter EditItems(IItemAdapter item)
        {
            return new ItemAdapter(_adaptee.EditItems(new[] { item.RevId }).First());
        }

        public IItemAdapter GetItemsById(long[] itemsIds)
        {
            return new ItemAdapter(_adaptee.GetItemsByIds(itemsIds).FirstOrDefault());
        }

        public void UpdateAndCommitItems(IItemAdapter[] itemsRevisions)
        {
            _adaptee.UpdateAndCommitItems(itemsRevisions.Select(
                item=>
                {
                    return item._adaptee;
                }).ToArray());
        }

        public void UpdateItemProperties(long[] itemsRevIds, IPropInstParamArrayAdapter[] propInstParamArray)
        {
            _adaptee.UpdateItemProperties(itemsRevIds, propInstParamArray.Select(prop =>
            {
                return prop._adaptee;
            }).ToArray());
        }
    }
}
