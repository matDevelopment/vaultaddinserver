﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PLMVaultExtensionCore.Interfaces;
using PLMVaultExtensionCore.Implementation;
using PLMVaultExtensionCore.VaultRepository.Adapters;
using PLMVaultExtensionCore.VaultRepository.Interfaces;
using System.Diagnostics;
using PLMVaultExtensionCore;
using System.Collections.Generic;
using Autodesk.Connectivity.WebServices;
using VDF = Autodesk.DataManagement.Client.Framework;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Properties;
using System.Linq;
using System.Threading;

namespace PLMVaultExtension_ModuleTests
{
    public class VaultItemPropertyTests
    {
        protected IConnectionAdapter connection;
        protected VaultPropertySetter sut;
        //private string serverName = "plmtest1544.cloudapp.net";
        //private string vaultName = "TestVault";
        //private string userName = "Administrator";
        //private string password = "";

        private string serverName = "localhost";
        private string vaultName = "TestVault";
        private string userName = "Administrator";
        private string password = "";
        //private const string ExistingPropertyName = "";
        private ILogInResultAdapter results;
        private Connection conn;
        private Item item;
        private Guid testGuid;
        private PropertyDefinitionDictionary propDefs;
        IConnectionManagerAdapter connectionManager;
        [TestInitialize]
        public void SetUp()
        {
            try
            {

                connectionManager = new ConnectionManagerAdapter();
                
                results = connectionManager.LogIn(serverName, vaultName, userName, password);
                
                AuthenticationFlags _authType = VDF.Vault.Currency.Connections.AuthenticationFlags.Standard;

                VDF.Vault.Results.LogInResult result = VDF.Vault.Library.ConnectionManager.LogIn(serverName, vaultName, userName, password, _authType, null);
                conn = result.Connection;
                propDefs = conn.PropertyManager.GetPropertyDefinitions(VDF.Vault.Currency.Entities.EntityClassIds.Items, null, VDF.Vault.Currency.Properties.PropertyDefinitionFilter.IncludeUserDefined);

                Cat[] categories = conn.WebServiceManager.CategoryService.GetCategoriesByEntityClassId("ITEM", true);
                long catId = -1;
                foreach (Cat category in categories)
                {
                    if (category.SysName == "Document")
                        catId = category.Id;
                }

               item = conn.WebServiceManager.ItemService.AddItemRevision(catId);

                // set the needed information
                item.Title = "Test Item";
                item.Detail = "Test Item";

                // save the item revision
                conn.WebServiceManager.ItemService.UpdateAndCommitItems(new Item[] { item });

                sut = new VaultPropertySetter();
            }
            catch (Exception exe)
            {
                Debug.WriteLine(exe.Message);
            }
        }

        [TestCleanup]
        public void TearDown()
        {
           // conn.WebServiceManager.ItemService.DeleteItems(new long[] { item.Id });
            connectionManager.LogOut();
        }


        [TestClass()]
        public class VaultItemPropertyWrapperTests : VaultItemPropertyTests
        {
            //[TestMethod]
            //public void PropertyDoesNotExist()
            //{

            //    var newEntityProperty = new EntityDataTest()
            //    {
            //        Id = item.Id, //Existing Item Id
            //        Type = "ITEM",
            //        Properties = new Dictionary<string, string>()
            //    };
            //    newEntityProperty.Properties.Add("testPropertyName", "testPropertyvalue");

            //    sut.SetProperty(new EntityDataTest[] { newEntityProperty }, results.Connection);
            //    VDF.Vault.Currency.Entities.ItemRevision itemRevision = new VDF.Vault.Currency.Entities.ItemRevision(conn, item);
            //    object propValue = conn.PropertyManager.GetPropertyValue(itemRevision, propDefs["testPropertyName"], null);
            //    Assert.IsTrue(propValue == "testPropertyvalue");
            //}

            //[TestMethod]
            //public void PropertyExists()
            //{
            //    var newEntityProperty = new EntityDataTest()
            //    {
            //        Id = item.Id, //Existing Item Id
            //        Type = "ITEM",
            //        Properties = new Dictionary<string, string>()
            //    };
            //    newEntityProperty.Properties.Add("_Kod_Towaru", "testPropertyvalue");

            //    sut.SetProperty(new EntityDataTest[] { newEntityProperty }, results.Connection);
            //    //VDF.Vault.Currency.Entities.ItemRevision itemRevision = new VDF.Vault.Currency.Entities.ItemRevision(conn, item);

            //    VDF.Vault.Library.ConnectionManager.CloseAllConnections();

            //    conn.WebServiceManager.PropertyService.Dispose();

            //    AuthenticationFlags _authType = VDF.Vault.Currency.Connections.AuthenticationFlags.Standard;
            //    VDF.Vault.Results.LogInResult result = VDF.Vault.Library.ConnectionManager.LogIn(serverName, vaultName, userName, password, _authType, null);
            //    conn = result.Connection;
            //    propDefs = conn.PropertyManager.GetPropertyDefinitions(VDF.Vault.Currency.Entities.EntityClassIds.Items, null, VDF.Vault.Currency.Properties.PropertyDefinitionFilter.IncludeUserDefined);

            //    var key = GetKey(propDefs);             
            //    PropInst[] values = conn.WebServiceManager.PropertyService.GetPropertiesByEntityIds("ITEM", new long[] { item.Id });
            //    PropInst value = values.First(x => x.PropDefId.Equals(key));

                
            //    //object propValue = conn.PropertyManager.GetPropertyValue(itemRevision, propDefs[key.ToString()], null);
            //    Assert.IsTrue(value.Val == "testPropertyvalue");

            //}

            private object GetKey(PropertyDefinitionDictionary propDefs)
            {
                foreach(var key in propDefs.Keys)
                {
                    if (propDefs[key].DisplayName.Equals("_Kod_Towaru"))
                        return propDefs[key].Id;
                
                }
                return null;
            }
        }
    }
    
    public class EntityDataTest : IEntityData
    {
        public string Type { get; set; }
        public long Id { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        
    }
}
