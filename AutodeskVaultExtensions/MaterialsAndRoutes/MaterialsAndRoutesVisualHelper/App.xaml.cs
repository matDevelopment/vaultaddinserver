﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;


// The extension ID needs to be unique for each extension.  
// Make sure to generate your own ID when writing your own extension. 


[assembly: Autodesk.Connectivity.Extensibility.Framework.ExtensionId("ED55229B-A0D5-4227-8FAB-6A0C3ECF1785")]

// This number gets incremented for each Vault release.
[assembly: Autodesk.Connectivity.Extensibility.Framework.ApiVersion("11.0")]

namespace PLMExtensionVisualHelper
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}
