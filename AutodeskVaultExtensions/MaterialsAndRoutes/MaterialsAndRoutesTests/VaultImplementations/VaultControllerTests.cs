﻿using Autodesk.Connectivity.Explorer.Extensibility;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PLMVaultExtensionCore;
using PLMVaultExtensionCore.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PLMExtension.VaultImplementations.Tests
{
    public class VaultControllerTests
    {
        private VaultController testSubject;
        private IEnumerable<VaultItem> vaultItems = new List<VaultItem>() {
            new VaultItem() {
                Id = 1,
                Properties = new Dictionary<string, string>() {
                    { "FirstProperty", "value1"},
                    { "SecondProperty", "value2"},
                }
            },
            new VaultItem() {
                Id = 2,
                Properties = new Dictionary<string, string>() {
                    { "FirstProperty", "value1"},
                    { "SecondProperty", "value2"},
                }
            }

        };

        [TestInitialize]
        public void Setup()
        {

            ISelection selection = new Mock<ISelection>().Object;
            IPropertySetter propertySetter = SetFakePropertySetter();
            Connection connection = SetFakeConnection();

            testSubject = new VaultController(selection, connection, propertySetter);
        }

        private IPropertySetter SetFakePropertySetter()
        {
            var propertySetterMoq = new Mock<IPropertySetter>();

            propertySetterMoq.Setup(x => x.SetProperty(It.IsAny<IEntityData[]>(), It.IsAny<IConnectionAdapter>()))
                .Callback((IEntityData[] entitiesData, IConnectionAdapter connection) =>
                {
                    foreach (var entityData in entitiesData) {
                        var selectedVaultItem = vaultItems.SingleOrDefault(x => x.Id == entityData.Id);
                        if (selectedVaultItem == null)
                        {
                            continue;
                        }
                        foreach (var property in entityData.Properties)
                        {
                            selectedVaultItem.Properties[property.Key] = property.Value;
                        }

                    }
                });
            return propertySetterMoq.Object;
        }

        private static Connection SetFakeConnection()
        {
            return new Mock<Connection>().Object;
        }

        //[TestClass()]
        //public class SetSinglePropertyTests : VaultControllerTests
        //{
        //    [TestMethod()]
        //    public void SetSinglePropertyTest()
        //    {

        //    }
        //}

    }

    internal class VaultItem
    {
        public int Id { get; set; }
        public Dictionary<string, string> Properties = new Dictionary<string, string>();
    }
}