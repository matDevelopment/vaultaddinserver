﻿/*=====================================================================
  
  This file is part of the Autodesk Vault API Code Samples.

  Copyright (C) Autodesk Inc.  All rights reserved.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/


using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Autodesk.Connectivity.Explorer.Extensibility;
using PLMExtension.Domain;
using PLMVaultExtensionCore;
using PLMVaultExtensionCore.Interfaces;
using PLMVaultExtensionCore.VaultRepository.Adapters;
using System.Collections.Generic;

namespace PLMExtension.VaultImplementations
{
    public class VaultController : IVaultController
    {
        IPropertySetter propertySetter;
        private ISelection selection;
        private IConnectionAdapter connection;

        public VaultController(ISelection selection, Connection connection, IPropertySetter propertySetter)
        {
            this.selection = selection;
            this.connection = new ConnectionAdapter(connection);
            this.propertySetter = propertySetter;
        }
        
        public void SetSingleProperty(string propertyName, string propertyValue)
        {
            var entityData = new ItemEntityData()
            {
                Id = selection.Id
            };
            entityData.Properties.Add(propertyName, propertyValue);

            var entitiesData = new List<IEntityData>() {
                entityData
            };

            propertySetter.SetProperty(entitiesData.ToArray(), connection);
        }
    }
}