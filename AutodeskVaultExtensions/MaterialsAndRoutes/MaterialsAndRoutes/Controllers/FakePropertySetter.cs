﻿using PLMExtension.Domain;
using PLMVaultExtensionCore;
using PLMVaultExtensionCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PLMExtension.VaultImplementations
{
    class FakePropertySetter : IPropertySetter
    {
        public void SetProperty(IEntityData[] entities, IConnectionAdapter connection)
        {
            MessageBox.Show($"Attempt to set property {entities[0].Properties[GlobalSettings.RoutePropertyName]}");
        }
    }
}
