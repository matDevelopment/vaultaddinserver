﻿using System.Collections.Generic;
using System.Windows;
using System.Collections.ObjectModel;
using PLMExtension.Domain;
using PLMExtension.VaultImplementations;
using System.Linq;
using PLMExtension.ViewModel;
using System;
using System.Windows.Forms.Integration;

namespace PLMExtension
{
    /// <summary>
    /// Interaction logic for MainWindows1.xaml
    /// </summary>
    public partial class MainWindow : Window, ITechnologyRoutesContainer
    {
        public ObservableCollection<TechnologyRouteViewModel> TechnologyRoutes { get; set; } = 
            new ObservableCollection<TechnologyRouteViewModel>(GlobalSettings.AvailableRoutes.Select(x => new TechnologyRouteViewModel() {
            Name = x.Name,
            OperationSummary = x.OperationSummary,
            Selected = false
        }));

        public Dictionary<string, string> AllOperations { get; } = GlobalSettings.AllOperations;

        public string AllOperationsDescription
        {
            get
            {
                return String.Join("\n", AllOperations.Select(x => String.Format("{0,-10} {1, 10}", x.Key, x.Value)));
            }
        }

        public IVaultController VaultContoller { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Cancel_BTN_Click(object sender, RoutedEventArgs e)
        {

            this.Close();
        }

        private void NewTechnologyRoute_BTN_Click(object sender, RoutedEventArgs e)
        {
            Window newTechnology = new NewTechnologyRoute(this);
            ElementHost.EnableModelessKeyboardInterop(newTechnology);

            newTechnology.Show();
        }

        private void OK_BTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string selectedRoutesValues = "";

                if (TechnologyRoutes != null && TechnologyRoutes.Any(x => x.Selected))
                {
                    selectedRoutesValues = TechnologyRoutes.Where(x => x.Selected)
                        .Select(x => x.Name)
                        .Aggregate((current, next) => $"{current};{next}");
                }
                VaultContoller.SetSingleProperty(GlobalSettings.RoutePropertyName, selectedRoutesValues);

                this.Close();
            }
            catch (Exception exe)
            {
                MessageBox.Show(exe.Message.ToString());
            }

        }
    }

}

