﻿using System.Collections.ObjectModel;

namespace PLMExtension.ViewModel
{
    public class SingleTechnologyViewModel : ViewModel
    {
        private string name = "";
        private string operationSummary = "";
        private string header;

        public ObservableCollection<OperationViewModel> Operations { get; set; } = new ObservableCollection<OperationViewModel>();
        public string OperationsSummary
        {
            get
            {
                return operationSummary;
            }
            set
            {
                operationSummary = value;
                NotifyPropertyChanged();
            }
        }

        public string Header
        {
            get => header;
            set
            {
                header = value;
                NotifyPropertyChanged();
            }
        }

        public string Name
        {
            get => name;
            set
            {
                name = value;
                NotifyPropertyChanged();
            }
        }
    }
}
