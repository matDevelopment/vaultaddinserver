﻿namespace PLMExtension.ViewModel
{
    public class TechnologyRouteViewModel : ViewModel
    {
        private string name;
        private bool selected = true;
        private string operationSummary;

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged();
            }
        }

        public bool Selected
        {
            get { return selected; }
            set
            {
                selected = value;
                NotifyPropertyChanged();
            }
        }

        public string OperationSummary
        {
            get { return operationSummary; }
            set
            {
                operationSummary = value;
                NotifyPropertyChanged();
            }
        }
    }

}

