﻿
using PLMExtension.Domain;

namespace PLMExtension.ViewModel
{
    public class OperationViewModel : ViewModel
    {
        private Operation operation;
        private bool selected;
        public Operation Operation
        {
            get => operation;
            set => operation = value;

        }
        public bool Selected
        {
            get => selected;
            set
            {
                selected = value;
                NotifyPropertyChanged();
            }
        }

    }
}
