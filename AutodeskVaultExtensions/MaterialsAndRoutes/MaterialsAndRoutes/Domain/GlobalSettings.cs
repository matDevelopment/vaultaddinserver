﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMExtension.Domain
{
    public static class GlobalSettings
    {
        public static string RoutePropertyName { get; } = "Marszruta";
        public static Dictionary<string, string> AllOperations { get; } = new Dictionary<string, string>()
        {
            { "BG","Gilotyna" },
            { "BP","Krawędziarka HFP" },
            { "ZZ","Zgrzewarka 1" },
            { "BL","Laser" },
            { "BB","Krawędziarka HFBO" },
            { "BR","Gratowanie" },
            { "SA","Spawarka 1" },
            { "BC","P4 Centrum gnące" },
            { "ZS","Zgrzewarka stacjonarna" },
            { "LA","Lakiernia proszkowa" },
            { "GP","Piła profili" },
            { "RA","Spawarka 1" },
            { "KW","Wiertarka stołowa" },
            { "DG","Stanowisko szyb zespolonych" },
            { "KA","Przygotówka Montaż" },
        };

        public static List<TechnologyRoute> AvailableRoutes { get; set; } = new List<TechnologyRoute>
                  {
                    new TechnologyRoute{
                        Name = "M000011/11",
                        OperationSummary = "BG.BP.ZZ",
                    },
                    new TechnologyRoute{
                        Name = "M000145/12",
                        OperationSummary = "BL.BB.BR.EA.MA",
                    },
                    new TechnologyRoute{
                        Name = "M000576/15",
                        OperationSummary = "BL.BB.SA.BR.ZZ",
                    },
                    new TechnologyRoute{
                        Name = "M000615/15",
                        OperationSummary = "BL.BC.ZS.LA.OB",
                    },
                    new TechnologyRoute{
                        Name = "M000343/13",
                        OperationSummary = "JA.KW.DG",
                    },
                    new TechnologyRoute{
                        Name = "M000343/13",
                        OperationSummary = "JA.KW.DG",
                    },
                    new TechnologyRoute{
                        Name = "M000185/12",
                        OperationSummary = "SA.KA.MA",
                    }
            };
    }
}
