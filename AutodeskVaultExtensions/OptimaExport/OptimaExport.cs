﻿/*=====================================================================
  
  This file is part of the Autodesk Vault API Code Samples.
  Copyright (C) Autodesk Inc.  All rights reserved.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

using Autodesk.Connectivity.Explorer.Extensibility;
using VDF = Autodesk.DataManagement.Client.Framework;
using System.Diagnostics;

using PLMVaultExtensionCore.Implementation;
using PLMVaultExtensionCore.Interfaces;

// These 5 assembly attributes must be specified or your extension will not load. 
//[assembly: AssemblyCompany("MAT")]

//[assembly: AssemblyProduct("OptimaExport")]
//[assembly: AssemblyDescription("Extension to export data to ERP Database")]

// The extension ID needs to be unique for each extension.  
// Make sure to generate your own ID when writing your own extension. 

[assembly: Autodesk.Connectivity.Extensibility.Framework.ExtensionId("B1BCF2E9-C703-45BD-A91E-4813D6AD8148")]

// This number gets incremented for each Vault release.
[assembly: Autodesk.Connectivity.Extensibility.Framework.ApiVersion("12.0")]

namespace OptimaExport
{
    public class OptimaExport : IExplorerExtension
    {
        public IEnumerable<CommandSite> CommandSites()
        {

            // Create the Hello World command object.
            CommandItem OptimaExportCmdItem = new CommandItem("OptimaExportCommand", "Optima Export")
            {
                // this command is active when a File is selected
                NavigationTypes = new SelectionTypeId[] { SelectionTypeId.Item, SelectionTypeId.Item },

                // this command is not active if there are multiple entities selected
                MultiSelectEnabled = false
            };

            // The HelloWorldCommandHandler function is called when the custom command is executed.
            OptimaExportCmdItem.Execute += OptimaExportCommandCommandHandler;

            // Create a command site to hook the command to the Advanced toolbar
            CommandSite toolbarCmdSite = new CommandSite("OptimaExportCommand.Toolbar", "Optima Export")
            {
                Location = CommandSiteLocation.AdvancedToolbar,
                DeployAsPulldownMenu = false
            };
            toolbarCmdSite.AddCommand(OptimaExportCmdItem);

            // Create another command site to hook the command to the right-click menu for Files.
            CommandSite itemContextCmdSite = new CommandSite("OptimaExportCommand.ItemContextMenu", "Optima Export")
            {
                Location = CommandSiteLocation.ItemContextMenu,
                DeployAsPulldownMenu = false
            };
            itemContextCmdSite.AddCommand(OptimaExportCmdItem);

            // Now the custom command is available in 2 places.

            // Gather the sites in a List.
            List<CommandSite> sites = new List<CommandSite>();
            sites.Add(toolbarCmdSite);
            sites.Add(itemContextCmdSite);

            // Return the list of CommandSites.
            return sites;
        }

        public IEnumerable<CustomEntityHandler> CustomEntityHandlers()
        {
            return null;
        }

        public IEnumerable<DetailPaneTab> DetailTabs()
        {// Create a DetailPaneTab list to return from method
            List<DetailPaneTab> fileTabs = new List<DetailPaneTab>();
            return fileTabs;
        }

        public IEnumerable<string> HiddenCommands()
        {
            return null;
        }

        public void OnLogOff(IApplication application)
        {
          
        }

        public void OnLogOn(IApplication application)
        {
            
        }

        public void OnShutdown(IApplication application)
        {
           
        }

        public void OnStartup(IApplication application)
        {
          
        }

        void OptimaExportCommandCommandHandler(object s, CommandItemEventArgs e)
        {
            try
            {
                VDF.Vault.Currency.Connections.Connection connection = e.Context.Application.Connection;

                // The Context part of the event args tells us information about what is selected.
                // Run some checks to make sure that the selection is valid.
                if (e.Context.CurrentSelectionSet.Count() == 0)
                    MessageBox.Show("Nothing is selected");
                else if (e.Context.CurrentSelectionSet.Count() > 1)
                    MessageBox.Show("This function does not support multiple selections");
                else
                {
                    e.Context.ForceRefresh = true;
                    // we only have one item selected, which is the expected behavior
                 
                    ISelection selection = e.Context.CurrentSelectionSet.First();

                    IPropertySetter propSetter = new VaultPropertySetter();

                   

                }
            }
            catch (Exception ex)
            {
                // If something goes wrong, we don't want the exception to bubble up to Vault Explorer.
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
