﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErpExport
{
    [Serializable()]
    public class PLMConfig
    {
        public string BomExportUri {get;set;}
        public string GetCaseIdUri { get; set; }
        public string GetAllCases { get;  set; }
        public string GetServerAdress { get;  set; }
    }
}
