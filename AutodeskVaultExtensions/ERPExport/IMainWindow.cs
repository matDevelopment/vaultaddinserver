﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using PLMExtension.ViewModel;

namespace PLMExtension
{
    public interface ITechnologyRoutesContainer
    {
        Dictionary<string, string> AllOperations { get; }
        string AllOperationsDescription { get; }
        ObservableCollection<TechnologyRouteViewModel> TechnologyRoutes { get; set; }

    }
}