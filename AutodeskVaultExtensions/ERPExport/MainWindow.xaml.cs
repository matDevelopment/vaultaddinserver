﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Autodesk.Connectivity.Explorer.Extensibility;
using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using ErpExport;

namespace ErpExport
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }
        ErpExportCommandExtension ErpExportClass = new ErpExportCommandExtension();
        public long idToSend;
        public List<string> CaseList = new List<string>();
        public string caseName;
        public Connection vaultConnection;

       public void Fillcombo()
        {
            CaseListBox.Items.Add("Żaden");
            foreach (var item in CaseList)
            {
                CaseListBox.Items.Add(item);
            }
           
        }
        private void Cancel_BTN_Click(object sender, RoutedEventArgs e)
        {
           
            this.Close();
        }

        private void OK_BTN_Click(object sender, RoutedEventArgs e)
        {

           int caseId = ErpExportClass.GetCaseIdFromName(caseName);
            //Wysyłanie exportu!!!

           // Task<bool> sentRequest = ErpExportClass.SetRequestToExportErp(idToSend, caseId, vaultConnection);
            Task<bool> sentRequest = ErpExportClass.SetRequestToExportErp(idToSend, caseId);
                bool result = sentRequest.Result;
                if (result)
                {
                    MessageBox.Show("Exported successfully");
                }
                else
                {
                    MessageBox.Show("Export not successfull");
                }
          
            this.Close();

        }

       

        private void ErpExport_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            
        }

        private void CaseListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            caseName = CaseListBox.SelectedItem.ToString();
        }
    }
}
