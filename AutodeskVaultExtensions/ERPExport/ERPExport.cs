﻿/*=====================================================================
  
  This file is part of the Autodesk Vault API Code Samples.
  Copyright (C) Autodesk Inc.  All rights reserved.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Autodesk.Connectivity.Explorer.Extensibility;
using Autodesk.Connectivity.WebServices;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using System.Net;
using System.Net.Http;
using VDF = Autodesk.DataManagement.Client.Framework;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using PLMExtension.VaultImplementations;
using PLMVaultExtensionCore.Implementation;
using PLMVaultExtensionCore.Interfaces;
using Newtonsoft.Json;
using System.Collections;
using System.Threading;
using System.Text;
using System.Xml;
using Autodesk.DataManagement.Client.Framework.Currency;


// These 5 assembly attributes must be specified or your extension will not load. 
[assembly: AssemblyCompany("MAT")]
[assembly: AssemblyProduct("ERPExport")]
[assembly: AssemblyDescription("New ERP Export for PLM")]

// The extension ID needs to be unique for each extension.  
// Make sure to generate your own ID when writing your own extension. 
[assembly: Autodesk.Connectivity.Extensibility.Framework.ExtensionId("EEEE59B2-F6FB-4C93-BEC2-3DA5EE549BCD")]

// This number gets incremented for each Vault release.
[assembly: Autodesk.Connectivity.Extensibility.Framework.ApiVersion("12.0")]


namespace ErpExport
{
    /// <summary>
    /// This class implements the IExtension interface, which means it tells Vault Explorer what 
    /// commands and custom tabs are provided by this extension.
    /// </summary>
    public class ErpExportCommandExtension : IExplorerExtension
    {

        private BOM bom;
        private Connection connection;
        public Connection excelExportConnection;
        public ISelection NewSelection;
        public List<SimplifiedCaseModel> FolderList;



        #region IExtension Members

        /// <summary>
        /// This function tells Vault Explorer what custom commands this extension provides.
        /// Part of the IExtension interface.
        /// </summary>
        /// <returns>A collection of CommandSites, which are collections of custom commands.</returns>
        public IEnumerable<CommandSite> CommandSites()
        {
           
            // Create the Hello World command object.
            CommandItem commandItem = new CommandItem("ErpExportCommand", "Export to ERP")
            {
                // this command is active when a File is selected
                NavigationTypes = new SelectionTypeId[] { SelectionTypeId.Item },

                // this command is not active if there are multiple entities selected
                MultiSelectEnabled = false
            };

            // The HelloWorldCommandHandler function is called when the custom command is executed.
            commandItem.Execute += ErpExportCommandHandler;

            // Create a command site to hook the command to the Advanced toolbar
            CommandSite toolbarCmdSite = new CommandSite("ErpExport.Toolbar", "Export to ERP")
            {
                Location = CommandSiteLocation.AdvancedToolbar,
                DeployAsPulldownMenu = false
            };
            toolbarCmdSite.AddCommand(commandItem);

            // Create another command site to hook the command to the right-click menu for Files.
            CommandSite fileContextCmdSite = new CommandSite("ErpExport.FileContextMenu", "Export to ERP")
            {
                Location = CommandSiteLocation.FileContextMenu,
                DeployAsPulldownMenu = false
            };
            fileContextCmdSite.AddCommand(commandItem);

            // Now the custom command is available in 2 places.

            // Gather the sites in a List.
            List<CommandSite> sites = new List<CommandSite>();
            sites.Add(toolbarCmdSite);
            sites.Add(fileContextCmdSite);

            // Return the list of CommandSites.
            return sites;
        }


        /// <summary>
        /// This function tells Vault Explorer what custom tabs this extension provides.
        /// Part of the IExtension interface.
        /// </summary>
        /// <returns>A collection of DetailTabs, each object represents a custom tab.</returns>
        public IEnumerable<DetailPaneTab> DetailTabs()
        {
            // Create a DetailPaneTab list to return from method
            List<DetailPaneTab> fileTabs = new List<DetailPaneTab>();

            // Create Selection Info tab for Files


            // Return tabs
            return fileTabs;
        }




        /// <summary>
        /// This function is called after the user logs in to the Vault Server.
        /// Part of the IExtension interface.
        /// </summary>
        /// <param name="application">Provides information about the running application.</param>
        public void OnLogOn(IApplication application)
        {
        }

        /// <summary>
        /// This function is called after the user is logged out of the Vault Server.
        /// Part of the IExtension interface.
        /// </summary>
        /// <param name="application">Provides information about the running application.</param>
        public void OnLogOff(IApplication application)
        {
        }

        /// <summary>
        /// This function is called before the application is closed.
        /// Part of the IExtension interface.
        /// </summary>
        /// <param name="application">Provides information about the running application.</param>
        public void OnShutdown(IApplication application)
        {
            // Although this function is empty for this project, it's still needs to be defined 
            // because it's part of the IExtension interface.
        }

        /// <summary>
        /// This function is called after the application starts up.
        /// Part of the IExtension interface.
        /// </summary>
        /// <param name="application">Provides information about the running application.</param>
        public void OnStartup(IApplication application)
        {
            // Although this function is empty for this project, it's still needs to be defined 
            // because it's part of the IExtension interface.
        }

        /// <summary>
        /// This function tells Vault Exlorer which default commands should be hidden.
        /// Part of the IExtension interface.
        /// </summary>
        /// <returns>A collection of command names.</returns>
        public IEnumerable<string> HiddenCommands()
        {
            // This extension does not hide any commands.
            return null;
        }

        /// <summary>
        /// This function allows the extension to define special behavior for Custom Entity types.
        /// Part of the IExtension interface.
        /// </summary>
        /// <returns>A collection of CustomEntityHandler objects.  Each object defines special behavior
        /// for a specific Custom Entity type.</returns>
        public IEnumerable<CustomEntityHandler> CustomEntityHandlers()
        {
            // This extension does not provide special Custom Entity behavior.
            return null;
        }

        #endregion

        
        /// <summary>
        /// This is the function that is called whenever the custom command is executed.
        /// </summary>
        /// <param name="s">The sender object.  Usually not used.</param>
        /// <param name="e">The event args.  Provides additional information about the environment.</param>
        void ErpExportCommandHandler(object s, CommandItemEventArgs e)
        {
            try
            {
                connection = e.Context.Application.Connection;


                // The Context part of the event args tells us information about what is selected.
                // Run some checks to make sure that the selection is valid.
                if (e.Context.CurrentSelectionSet.Count() == 0)
                    MessageBox.Show("Nothing is selected");
                else if (e.Context.CurrentSelectionSet.Count() > 1)
                    MessageBox.Show("This function does not support multiple selections");
                else
                {
                    e.Context.ForceRefresh = true;
                    MainWindow window = new MainWindow();
                    var vaultWindowHandle = Process.GetCurrentProcess().MainWindowHandle; //Vault window handle
                    new WindowInteropHelper(window) { Owner = vaultWindowHandle };
                    window.vaultConnection = connection;


                    // we only have one item selected, which is the expected behavior
                    ISelection selection = e.Context.CurrentSelectionSet.First();
                    NewSelection = selection;

                    if (selection.TypeId == SelectionTypeId.Item)
                    {

                        Task<List<SimplifiedCaseModel>> sentRequest = SetRequestToGetAllCases();
                        FolderList = sentRequest.Result;

                        foreach (var folder in FolderList)
                        {
                            window.CaseList.Add(folder.Name);
                        }

                        window.idToSend = selection.Id;
                        window.Fillcombo();
                        window.Show();
                    }


                    //if (selection.TypeId == SelectionTypeId.Item)


                    //    ////Wysyłanie exportu!!!
                    //    //{
                    //    //Task<bool> sentRequest = SetRequestToExportErp(selection.Id);
                    //    //bool result = sentRequest.Result;
                    //    //if (result)
                    //    //{
                    //    //    MessageBox.Show("Exported successfully");
                    //    //}
                    //    //else
                    //    //{
                    //    //    MessageBox.Show("Export not successfull");
                    //    //}
                    //    //}




                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        public void AddExcelToVault(string folderPath, string filepath)
        {
            var configUri = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\..\..\..\DocumentsRepository\Config\{Environment.MachineName}-ServerConfig.xml";
            string xmlText = System.IO.File.ReadAllText(configUri);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlText);
            XmlNode serverNode = doc.DocumentElement.SelectSingleNode("ServerName");
            XmlNode vaultNode = doc.DocumentElement.SelectSingleNode("VaultName");
            XmlNode userNode = doc.DocumentElement.SelectSingleNode("UserName");
            XmlNode passwordNode = doc.DocumentElement.SelectSingleNode("Password");

            VDF.Vault.Results.LogInResult results = VDF.Vault.Library.ConnectionManager.LogIn(
               serverNode.InnerText.ToString(), vaultNode.InnerText.ToString(), userNode.InnerText.ToString(), passwordNode.InnerText.ToString(), VDF.Vault.Currency.Connections.AuthenticationFlags.Standard, null
               );

            Connection conn = results.Connection;

            Folder serviceFolder = conn.WebServiceManager.DocumentService.GetFolderByPath(folderPath);
            VDF.Vault.Currency.Entities.Folder folder = new VDF.Vault.Currency.Entities.Folder(conn, serviceFolder);
            FileAssocParam[] mockParams = new FileAssocParam[0];
            BOM mockBom = new BOM();
            FilePathAbsolute filePathAbsolute = new FilePathAbsolute(filepath);
            conn.FileManager.AddFile(folder, "", mockParams, mockBom, FileClassification.None, false, filePathAbsolute);
        }
        public void GetFolderExportPath(long itemId, Connection connection2)
        {
            Folder[] folders = connection2.WebServiceManager.DocumentService.GetFoldersByFileMasterId(itemId);
        }
        public int GetCaseIdFromName(string name)
        {
           string[] parameters =  name.Split(':');
            int id;
            Int32.TryParse(parameters[0], out id);
            return id;
        }
        public async Task<bool> SetRequestToExportErp(long id, int caseId)
        {
            try
            {
               HttpClient client = new HttpClient();

                //string URI = "http://localhost:81/api/Bom/SaveBom";
                string uri = GetPlmBomExportPath();
               
                string htmlResult;
                if (String.IsNullOrEmpty(uri))
                {
                    throw new Exception("No plm export url set in configuration file");
                }
               // GetFolderExportPath(id, connetionToVault);
                ERPExportModel export = new ERPExportModel();
                export.fileId = id;
                export.caseId = caseId;
              
               
                var data = JsonConvert.SerializeObject(export);
               // using (WebClient wc = new WebClient())
                //{
                //    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                //    wc.UseDefaultCredentials = true;
                //    wc.Encoding = Encoding.UTF8;

                //    htmlResult = wc.UploadString(uri, data);
                //}
                using (ExtendedWebClient wc = new ExtendedWebClient(new System.Uri(uri)))
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.UseDefaultCredentials = true;
                    wc.Encoding = Encoding.UTF8;

                    htmlResult = wc.UploadString(uri, data);
                }
                return htmlResult == export.fileId.ToString();


            }
            catch (Exception exe)
            {
                //todo logging
                MessageBox.Show(exe.Message);
                throw exe;
            }

        }                    
       
        public async Task<List<SimplifiedCaseModel>> SetRequestToGetAllCases(
            CancellationToken cancelToken = default(CancellationToken))
        {
            
            try
            {
                var uri = GetAllCases(); // nie używaj konkatenacji tylko interpolacji

                // najpierw sprawdzamy warunki wejściowe, potem tworzymy obiekty
                if (String.IsNullOrEmpty(uri))
                {
                    throw new Exception("No plm export url set in configuration file");
                }

                using (var client2 = new HttpClient()) // korzystajmy z dobrodziejstwa IDisposable
                {
                    client2.BaseAddress = new Uri(GetServerAdress(), UriKind.Absolute);
                    client2.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
                   
                    var response =  client2.GetAsync(uri, cancelToken).Result; // to jest ok
                    var json =  response.Content.ReadAsStringAsync().Result; // dostajesz zwrotke w postaci 200,404,501, etc - reszte trzeba sobie wyłuskać
                    var result = JsonConvert.DeserializeObject<List<SimplifiedCaseModel>>(json); // z Geta wcale nie musi przyjsc jeden wynik
                   
                    
                    return result;
                }
            }
            catch (Exception exe)
            {
                throw exe; // ExceptionDrivenDevelopment
            }

        }
        private static string GetPlmBomExportPath()
        {
            string directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var configUri = directory + @"\config.xml";
            XmlSerializer serializer = new XmlSerializer(typeof(PLMConfig));
            StreamReader reader = new StreamReader(configUri);            
            PLMConfig config = (PLMConfig)serializer.Deserialize(reader);
            reader.Close();            
            return config.BomExportUri;
        }
        private static string GetServerAdress()
        {
            string directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var configUri = directory + @"\config.xml";
            XmlSerializer serializer = new XmlSerializer(typeof(PLMConfig));
            StreamReader reader = new StreamReader(configUri);           
            PLMConfig config = (PLMConfig)serializer.Deserialize(reader);
            reader.Close();            
            return  config.GetServerAdress;
        }
        private static string GetCaseIdPath()
        {
            string directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var configUri = directory + @"\config.xml";
            XmlSerializer serializer = new XmlSerializer(typeof(PLMConfig));
            StreamReader reader = new StreamReader(configUri);            
            PLMConfig config = (PLMConfig)serializer.Deserialize(reader);
            reader.Close();           
            return config.GetCaseIdUri;
        }
        private static string GetAllCases()
        {
            string directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var configUri = directory + @"\config.xml";
            XmlSerializer serializer = new XmlSerializer(typeof(PLMConfig));
            StreamReader reader = new StreamReader(configUri);            
            PLMConfig config = (PLMConfig)serializer.Deserialize(reader);
            reader.Close();            
            return config.GetAllCases;
        }

        ////private BOM GetBomByFileId(long id)
        ////{
        ////    return connection.WebServiceManager.DocumentService.GetBOMByFileId(id);
        ////}

        /// <summary>
        /// This function is called whenever our custom tab is active and the selection has changed in the main grid.
        /// </summary>
        /// <param name="sender">The sender object.  Usually not used.</param>
        /// <param name="e">The event args.  Provides additional information about the environment.</param>
        void propertyTab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // The event args has our custom tab object.  We need to cast it to our type.
                MyCustomTabControl tabControl = e.Context.UserControl as MyCustomTabControl;

                // Send selection to the tab so that it can display the object.
                tabControl.SetSelectedObject(e.Context.SelectedObject);
            }
            catch (Exception ex)
            {
                // If something goes wrong, we don't want the exception to bubble up to Vault Explorer.
                MessageBox.Show("Error: " + ex.Message);
            }
        }


    }
}
