﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsRepository
{
    public interface IDocumentsServerConfig
    {
        string ServerName { get; set; }
        string VaultName { get; set; }
        long CasesRootFolderId { get; set; }
        long TemplatesRootFolderId { get; set; }
        string UserName { get; set; }
        string Password { get; set; }

    }
}
