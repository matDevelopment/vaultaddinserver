﻿namespace VaultWrapper.VaultRepository.Interfaces
{
    public interface IDocumentServiceAdapter
    {
        IFolderAdapter AddFolder(string newFolderName, long rootFolderId, bool IsLib);
        void DeleteFolderHierarchy(long id);
        void Dispose();
        IFolderAdapter GetFolderById(long id);
        IFolderAdapter GetFolderByPath(string path);
        IFolderAdapter GetFolderRoot();
        IFolderAdapter[] GetFoldersByParentId(long parentId, bool recurse);
    }
}