﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public interface IVaultBom
    {
        long Id { get; }
        string LevelIndicator { get; }
        string Name { get; }
        bool IsTopLevelAssembly { get; }
        IEnumerable<BomValue> GetBomValues();
        IEnumerable<IVaultBom> GetChildrenBoms();
        IEnumerable<BomValueDefiniton> GetHeaders();
        IEnumerable<string[]> ToFlatTable(string[] headers);

    }
    public class BomValue
    {
        public BomValueDefiniton Definiton = new BomValueDefiniton();
        public string Name
        {
            get
            {
                return Definiton.Name;
            }
            set
            {
                Definiton.Name = value;
            }
        }
        public string DisplayName
        {
            get
            {
                return Definiton.DisplayName;
            }
            set
            {
                Definiton.DisplayName = value;
            }
        }

        public string Value { get; set; }

        public override string ToString()
        {
            return String.Format("{0,-35} {1,-35} {2,-35}", Name, DisplayName, Value);
        }
    }
    public class BomValueDefiniton
    {
        public string Name;
        public string DisplayName;

        public override bool Equals(Object obj)
        {
            return obj is BomValueDefiniton && this == (BomValueDefiniton)obj;
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        public static bool operator ==(BomValueDefiniton x, BomValueDefiniton y)
        {
            return String.Equals(x.Name, y.Name);
        }
        public static bool operator !=(BomValueDefiniton x, BomValueDefiniton y)
        {
            return !(x == y);
        }

    }



}
