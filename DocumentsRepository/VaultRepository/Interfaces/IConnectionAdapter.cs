﻿using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using System;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public interface IConnectionAdapter : IDisposable
    {
        IDocumentServiceAdapter DocumentService { get; }
        IWebServiceManager BomService { get; }

    }
}