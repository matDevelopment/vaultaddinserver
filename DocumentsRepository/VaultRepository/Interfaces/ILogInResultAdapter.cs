﻿using System.Collections.Generic;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public interface ILogInResultAdapter
    {
        IConnectionAdapter Connection { get; }
        Dictionary<string, string> ErrorMessages { get; }
        bool Success { get; }
    }
}