﻿using System;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Autodesk.DataManagement.Client.Framework.Vault.Results;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public interface IConnectionManagerAdapter
    {

        ILogInResultAdapter LogIn(string serverName, string vaultName, string userName, string password);
        bool LogOut();
    }
}