﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Connectivity.WebServices;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public interface IWebServiceManager
    {
        Item GetItemById(long itemId);
        ItemFileAssoc GetConnectedFile(long itemId);
         bool CheckIfItemHasFiles(long itemId);
        FileAssocLite[] GetAssociatedParentFiles(long fileId);
        PropDef[] GetPropertyDefinitionsForItem();
        PropInst[] GetItemPropertiesById(long itemId);
        ItemAssoc[] GetBOMAssociationsByItemId(long itemId);
    }
}
