﻿using System.Collections.Generic;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public interface IFolderAdapter
    {
        string FullName { get; }
        long Id { get; }
        bool IsLib { get; }
        string Name { get; }
        IEnumerable<IFolderAdapter> GetChildrenFolders();
    }
}