﻿using Autodesk.Connectivity.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public class FolderAdapter : IFolderAdapter
    {
        private Folder _adaptee;
        private readonly IDocumentServiceAdapter _documentService;
        private List<IFolderAdapter> ChildrenFolders = null;

        public FolderAdapter(Folder adaptee, IDocumentServiceAdapter documentService)
        {
            _adaptee = adaptee;
            this._documentService = documentService;
        }

        public bool IsLib
        {
            get
            {
                return _adaptee.IsLib;
            }
        }
        public long Id
        {
            get
            {
                return _adaptee.Id;
            }
        }

        public string Name
        {
            get
            {
                return _adaptee.Name;
            }
        }
        public string FullName
        {
            get
            {
                return _adaptee.FullName;
            }
        }

        public IEnumerable<IFolderAdapter> GetChildrenFolders()
        {
            try
            {
                if (ChildrenFolders == null)
                {
                    ReadChildFoldersFromVault();
                }
                return ChildrenFolders;
            }
            catch (VaultServiceCommunicationException exe)
            {
                throw new ApplicationException("Document service is not extablished. Please log into Vault and establish valid Document Service",exe);
            }
            
        }

        private void ReadChildFoldersFromVault()
        {
            ChildrenFolders = new List<IFolderAdapter>();
            var childrenFolders = _documentService.GetFoldersByParentId(_adaptee.Id, false);
            if (childrenFolders == null)
            {
                return;
            }
            foreach (var child in childrenFolders)
            {
                ChildrenFolders.Add(child);
            }
        }
    }
}
