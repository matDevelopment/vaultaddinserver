﻿using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VaultWrapper.VaultRepository.Adapters;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public class ConnectionAdapter : IDisposable, IConnectionAdapter
    {
        public Connection Adaptee { get; private set; }

        public ConnectionAdapter(Connection adaptee)
        {
            Adaptee = adaptee;
        }

        public IDocumentServiceAdapter DocumentService
        {
            get
            {
                return new DocumentServiceAdapter(Adaptee.WebServiceManager.DocumentService);
            }
        }

        public IWebServiceManager BomService
        {
            get
            {
                return new WebServiceManagerAdapter(Adaptee.WebServiceManager);
            }
        }


        public void Dispose()
        {
            if (Adaptee != null && Adaptee.WebServiceManager != null)
            {
                Adaptee.WebServiceManager.Dispose();
            }
        }
    }
}
