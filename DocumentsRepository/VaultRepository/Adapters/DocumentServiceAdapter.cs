﻿using Autodesk.Connectivity.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public class DocumentServiceAdapter : IDisposable, IDocumentServiceAdapter
    {
        private DocumentService _adaptee;

        public DocumentServiceAdapter(DocumentService adaptee)
        {
            _adaptee = adaptee;
        }

        public IFolderAdapter GetFolderByPath(string path )
        {
            var folder = _adaptee.GetFolderByPath(path);
            if (folder == null)
                return null;

            return new FolderAdapter(folder, this);
        }

        public IFolderAdapter GetFolderById(long id)
        {

                var folder = _adaptee.GetFolderById(id); 
                if (folder == null)
                    return null;
            return new FolderAdapter(folder, this);

        }

        public IFolderAdapter GetFolderRoot()
        {
            var folder = _adaptee.GetFolderRoot();
            if (folder == null)
                return null;

            return new FolderAdapter(folder, this);
        }

        public IFolderAdapter[] GetFoldersByParentId(long parentId, bool recurse)
        {
            return _adaptee.GetFoldersByParentId(parentId, recurse)?.Select(folder => new FolderAdapter(folder, this)).ToArray();
        }

        public void DeleteFolderHierarchy(long id)
        {
            _adaptee.DeleteFolderHierarchy(id);
        }

        public IFolderAdapter AddFolder(string newFolderName,long rootFolderId, bool IsLib)
        {
            try
            {
                return new FolderAdapter(_adaptee.AddFolder(newFolderName, rootFolderId, IsLib),this);
            }
            catch (VaultServiceErrorException exe)
            {
                switch (exe.ErrorCode)
                {
                    case 1000:
                        {
                            throw new ApplicationException("Template folder does not exist", exe);

                        }
                    case 1011:
                        {
                            throw new ApplicationException("Can not create a folder based on the template - folder already exists", exe);
                        }
                    default:
                        {
                            throw exe; ;
                        }
                }
            }
        }


        public void Dispose()
        {
            if (_adaptee != null)
            {
                _adaptee.Dispose();
            }
        }
    }
}
