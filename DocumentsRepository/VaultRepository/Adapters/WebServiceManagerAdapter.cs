﻿using Autodesk.Connectivity.WebServicesTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VaultWrapper.VaultRepository.Interfaces;
using Autodesk.Connectivity.WebServices;

namespace VaultWrapper.VaultRepository.Adapters
{
    public class WebServiceManagerAdapter : IWebServiceManager
    {
        private readonly WebServiceManager adaptee;

        public WebServiceManagerAdapter(WebServiceManager adaptee)
        {
            this.adaptee = adaptee;
        }

        public FileAssocLite[] GetAssociatedParentFiles(long fileId)
        {
            return adaptee.DocumentService.GetFileAssociationLitesByIds(new long[] { fileId }, FileAssocAlg.Actual, FileAssociationTypeEnum.Dependency, false, FileAssociationTypeEnum.None, false, false, false, false);
        }

        public ItemAssoc[] GetBOMAssociationsByItemId(long itemId)
        {

            return adaptee.ItemService.GetItemBOMAssociationsByItemIds(new long[] { itemId }, false);
        }

        //public ItemFileAssoc GetConnectedFile(long itemId)
        //{
        //    return adaptee.ItemService.GetItemFileAssociationsByItemIds(new long[] { itemId }, ItemFileLnkTypOpt.Primary).First();
        //}
        public ItemFileAssoc GetConnectedFile(long itemId)
        {
            return adaptee.ItemService.GetItemFileAssociationsByItemIds(new long[] { itemId }, ItemFileLnkTypOpt.Primary).FirstOrDefault();
        }

        public bool CheckIfItemHasFiles(long itemId)
        {
            if(adaptee.ItemService.GetItemFileAssociationsByItemIds(new long[] { itemId }, ItemFileLnkTypOpt.Primary).FirstOrDefault() == null)
            {
                return false;
            }
            else
            {
                return true;
            }
                    }
        public Item GetItemById(long itemId)
        {
            return adaptee.ItemService.GetItemsByIds(new long[] { itemId }).First();
        }

        public PropInst[] GetItemPropertiesById(long itemId)
        {
            return adaptee.PropertyService.GetPropertiesByEntityIds("ITEM", new long[] { itemId });
        }

        public PropDef[] GetPropertyDefinitionsForItem()
        {
            return adaptee.PropertyService.GetPropertyDefinitionsByEntityClassId("ITEM");
        }
    }
}
