﻿using Autodesk.DataManagement.Client.Framework.Vault.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VDF = Autodesk.DataManagement.Client.Framework;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Autodesk.DataManagement.Client.Framework.Vault.Results;
using Autodesk.Connectivity.WebServicesTools;
using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServices.WCF;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public class ConnectionManagerAdapter : IConnectionManagerAdapter
    {
        private IVaultConnectionManagerService _adaptee;
        private string _serverName;
        private string _fileServerName;
        private string _vaultName;
        private string _userName;
        private string _password;
        
        private AuthenticationFlags _authType;


        
        public void CloseAllConnections()
        {

            _adaptee.CloseAllConnections();
        }

        public ILogInResultAdapter LogIn(string serverName, string vaultName, string userName, string password)
        {
            try
            {
                //As per https://forums.autodesk.com/t5/vault-forum/vault-2018-web-app-login-issue/td-p/7273764
                // WebServiceManager.AllowUI = false;
                // LicenseManager.Instance.SetActive();
               // _adaptee = VDF.Vault.Library.ConnectionManager;
                _serverName = serverName;
                _fileServerName = serverName;
                _vaultName = vaultName;
                _userName = userName;
                _password = password;
                _authType = VDF.Vault.Currency.Connections.AuthenticationFlags.Standard;

                ServerIdentities mServerId = new ServerIdentities();
                mServerId.DataServer = _serverName;           
                mServerId.FileServer = _fileServerName;
                string mVaultName = vaultName;
                string mUserName = userName;
                string mPassword = password;

                LicensingAgent mLicAgent = LicensingAgent.Server;
                WebServiceManager mVault = null;
                UserPasswordCredentials mCred = null;


                mCred = new UserPasswordCredentials(mServerId, mVaultName, mUserName, mPassword, mLicAgent);
                mVault = new WebServiceManager(mCred);

                return new LogInResultAdapter(_adaptee.LogIn(serverName, vaultName, userName, password, _authType, null));
            }
            catch (Exception exe)
            {
                throw(exe);
            }
        }

        public bool LogOut()
        {
           return _adaptee.LogOut(VDF.Vault.Library.ConnectionManager.GetExistingConnection(_serverName,_vaultName,_userName, _password, _authType));
          
        }

    }
}
