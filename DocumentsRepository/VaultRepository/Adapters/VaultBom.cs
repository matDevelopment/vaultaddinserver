﻿using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace VaultWrapper.VaultRepository.Interfaces
{
    public class VaultBom : IVaultBom
    {
        protected List<BomValue> bomValues = new List<BomValue>();
        protected List<VaultBom> childrenBoms = new List<VaultBom>();

        private IWebServiceManager webService;
        private VaultBom parent;
        private int index;
        private Item selectedItem;

        public long Id { get; private set; }
        public string LevelIndicator { get; private set; } = "1";
        public string Name { get; private set; }
        public bool IsTopLevelAssembly { get; private set; }
        public ItemAssoc ItemAssociation { get; private set; }

        public IEnumerable<BomValue> GetBomValues()
        {
            return bomValues;
        }

        public IEnumerable<IVaultBom> GetChildrenBoms()
        {
            return childrenBoms;
            
        }

        public IEnumerable<BomValueDefiniton> GetHeaders()
        {
            List<BomValueDefiniton> headers = bomValues.Select(x => x.Definiton).ToList();
            foreach (var child in this.childrenBoms)
            {
                headers.AddRange(child.GetHeaders());
            }
            return headers.Distinct();
        }


        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="selectionId">Vault Item selection Id (not FILE)</param>
        /// <param name="bomService"></param>
        /// <param name="parent">Parent Vault item. null if top level</param>
        /// <param name="index">Index of child item inside parent. Indexing starting at 1;</param>
        public VaultBom(long selectionId, IWebServiceManager bomService, VaultBom parent = null, int index = 0)
        {
            this.Id = selectionId;
            this.webService = bomService;
            this.parent = parent;
            this.index = index;

        }
        /// <summary>
        /// Sets up children and bom values for object and all its children
        /// </summary>
        public void InitBom()
        {
            try
            {
                selectedItem = webService.GetItemById(Id);
                Name = selectedItem.ItemNum;
                if (webService.CheckIfItemHasFiles(selectedItem.Id))
                {
                    var fileConnected = webService.GetConnectedFile(selectedItem.Id);
                    IsTopLevelAssembly = CheckIfTopLevelAssembly(fileConnected.CldFileId);
                }
               
               

                SetBomValuesFromParent();
                InitBomValuesFromAssociation();
                SetBomValuesFromItemProperties(selectedItem);
                
                childrenBoms = SetAndInitChildrenBoms(selectedItem).ToList();
            }
            catch (VaultServiceErrorException exe)
            {
                if (exe.ErrorCode == 1013 || exe.ErrorCode == 1405)
                {
                    throw new FileNotFoundException($"Cound not find item with id {Id}", exe);
                }
                throw exe;
            }
        }

        private void SetBomValuesFromParent()
        {
            if (parent != null)
            {
                LevelIndicator = $"{parent.LevelIndicator}.{index}";
            }
            else {
                LevelIndicator = "1";
            }
            bomValues.Add(new BomValue()
            {
                Name = "BOMIndicator_Parent",
                DisplayName = "Parent",
                Value = parent?.Name
            });
            bomValues.Add(new BomValue()
            {
                Name = "BOMIndicator_Level",
                DisplayName = "Level",
                Value = LevelIndicator
            });
        }


        private VaultBom CreateBomForFileAssotiation(ItemAssoc assotiation, int indexInsideAssociation)
        {
            var item = new VaultBom(assotiation.CldItemID, webService, this, indexInsideAssociation +1); //+1 since array indexing starts with 0 and vault indexing starts with 1
            item.ItemAssociation = assotiation;
            item.InitBom();

            return item;
        }
        private void InitBomValuesFromAssociation()
        {
            if (ItemAssociation == null)
            {
                return;
            }
            bomValues.Add(new BomValue()
            {
                Name = "InstCount",
                DisplayName = "Item Qty",
                Value = ItemAssociation.InstCount.ToString()
            });
            bomValues.Add(new BomValue()
            {
                Name = "PositionNumber",
                DisplayName = "Position Number",
                Value = ItemAssociation.PositionNum
            });
            bomValues.Add(new BomValue()
            {
                Name = "Quantity",
                DisplayName = "Quantity",
                Value = ItemAssociation.Quant.ToString()
            });
            bomValues.Add(new BomValue()
            {
                Name = "BOMOrder",
                DisplayName = "Row Order",
                Value = ItemAssociation.BOMOrder.ToString()
            });
            bomValues.Add(new BomValue() //I am not sure if this is desired property
            {
                Name = "DetailID",
                DisplayName = "DetailID",
                Value = ItemAssociation.BOMCompId.ToString()
            });
            bomValues.Add(new BomValue()
            {
                Name = "UnitSize",
                DisplayName = "Unit Qty",
                Value = ItemAssociation.UnitSize.ToString()
            });

        }

        private bool CheckIfTopLevelAssembly(long fileId)
        {
            FileAssocLite[] associatedParentFiles = webService.GetAssociatedParentFiles(fileId); //, FileAssocAlg.Actual, FileAssociationTypeEnum.Dependency, false, FileAssociationTypeEnum.None, false, false, false, false);
            return associatedParentFiles != null && !associatedParentFiles.Any();
        }

        private void SetBomValuesFromItemProperties(Item item)
        {
            try
            {
                var propDefs = webService.GetPropertyDefinitionsForItem();
                var properties = webService.GetItemPropertiesById(item.Id);
            
                foreach (var property in properties)
                {
                    var propertyDef = propDefs.First(x => x.Id == property.PropDefId);
                    bomValues.Add(new BomValue()
                    {
                        Name = propertyDef.SysName,
                        DisplayName = propertyDef.DispName,
                        Value = property.Val?.ToString()
                    });
                }
            }
            catch (Exception exe)
            {
                //todo log error
                throw exe;
            }
        }

        private IEnumerable<VaultBom> SetAndInitChildrenBoms(Item item)
        {
            try
            {
                ItemAssoc[] itemAssociationArray = webService.GetBOMAssociationsByItemId(item.Id);
                if (itemAssociationArray == null)
                {
                    return new List<VaultBom>();
                }
                return itemAssociationArray.Select((itemAssoc, i) => CreateBomForFileAssotiation(itemAssoc, i));
            }
            catch (Exception exe)
            {
                throw exe;
            }
        }

        public IEnumerable<string[]> ToFlatTable(string[] headers)
        {
            var outputList = new List<string[]>();
            GetBomFlatTable(outputList, headers);
            return outputList;
        }

        private void GetBomFlatTable(ICollection<string[]> outputList, string[] headers)
        {
            var cellValues = new string[headers.Count()];
            for (int col = 0; col < headers.Count(); col++)
            {
                cellValues[col] = bomValues.FirstOrDefault(x => String.Equals(x.Name, headers[col], StringComparison.CurrentCulture))?.Value;
            }
            outputList.Add(cellValues);
            foreach (var childItem in childrenBoms)
            {
                childItem.GetBomFlatTable(outputList, headers);
            }

        }
    }
}
