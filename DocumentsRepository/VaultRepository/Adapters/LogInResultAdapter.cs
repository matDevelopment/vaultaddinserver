﻿using Autodesk.DataManagement.Client.Framework.Vault.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaultWrapper.VaultRepository.Interfaces
{
    public class LogInResultAdapter : ILogInResultAdapter
    {
        private LogInResult _adaptee;

        public LogInResultAdapter(LogInResult adaptee)
        {
            _adaptee = adaptee;
        }

        public bool Success { get { return _adaptee.Success; } }

        public IConnectionAdapter Connection { get { return new ConnectionAdapter(_adaptee.Connection); } }

        public Dictionary<string,string> ErrorMessages
        {
            get
            {
                return _adaptee.ErrorMessages
                    .ToDictionary(x => x.Key.ToString(), x => x.Value);
            }
        }

    }
}
