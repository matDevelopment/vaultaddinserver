﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DocumentsRepository
{
    public class DocumentsServerConfig : IDocumentsServerConfig
    {
        public static string EnvironmentName { get; set; } = Environment.MachineName;

        public static  string FileName
        {
            get
            {
                return $"{EnvironmentName}-ServerConfig.xml";
            }
        } 
        public static  string FilePath
        {
            get
            {
                return $@"{AppDomain.CurrentDomain.BaseDirectory}\Config\{FileName}";
            }
        } 

        public string ServerName { get ; set; }
        public string VaultName { get; set ; }
        public long CasesRootFolderId { get; set; }
        public long TemplatesRootFolderId { get; set; }
        public string UserName { get; set; }
        public string Password { get ; set ; }
        public int Test { get; set; }

        public static DocumentsServerConfig ReadFromConfigFile()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(DocumentsServerConfig));
            using (StreamReader reader = new StreamReader(FilePath))
                {
                    return  ((DocumentsServerConfig)serializer.Deserialize(reader));
                }
        }
        public static string CheckValuesFromConfigFile()
        {

            return $@"{AppDomain.CurrentDomain.BaseDirectory}\Config\{FileName}";
        }
    }
}