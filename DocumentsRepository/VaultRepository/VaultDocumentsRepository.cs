﻿using Autodesk.Connectivity.WebServices;

using Autodesk.Connectivity.WebServices.WCF;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using VDF = Autodesk.DataManagement.Client.Framework;
using System;
using System.Linq;
using VaultWrapper.VaultRepository.Adapters;
using VaultWrapper.VaultRepository.Interfaces;

namespace DocumentsRepository
{
    public class VaultDocumentsRepository : IDisposable, IDocumentsRepository
    {
       
        private IConnectionAdapter connection;
        private IDocumentServiceAdapter service;
        private ConnectionManagerAdapter connectionManager;
        private readonly IDocumentsServerConfig vaultConfig;
        private bool disposedValue = false; // To detect redundant calls
        public bool Initilized { get; private set; } = false;
        private long rootFolderId;

        public VaultDocumentsRepository(IDocumentsServerConfig vaultConfig)
        {
            //Initialize(vaultConfig.ServerName, vaultConfig.VaultName, vaultConfig.UserName, vaultConfig.Password, vaultConfig.VaultRootFolderId);
            this.vaultConfig = vaultConfig;
        }
        public void TryInitialize(string server, string vault, string user, string password, long rootFolderId)
        {
            if (!Initilized)
            {
                //connection = LogIn(server, vault, user, password);
                connection = LogIn(server, vault, user, password);
                service = connection.DocumentService;
                this.rootFolderId = rootFolderId;
                Initilized = true;
            }
        }

        public IFolderAdapter GetRootTemplatesFolder()
        {
            return GetFolder(vaultConfig.TemplatesRootFolderId, true);
        }


        public IFolderAdapter GetFolder(long folderId, bool getChildFoldersRecusivly)
        {
            TryInitialize(vaultConfig.ServerName, vaultConfig.VaultName, vaultConfig.UserName, vaultConfig.Password, vaultConfig.CasesRootFolderId);
            var sourceFolder = service.GetFolderById(folderId);
            if (getChildFoldersRecusivly)
            {
                GetChildFoldersRecursivly(sourceFolder);
            }
            return sourceFolder;
        }

        private void GetChildFoldersRecursivly(IFolderAdapter sourceFolder)
        {
            var children = sourceFolder.GetChildrenFolders();
            foreach (var child in children)
            {
                GetChildFoldersRecursivly(child);
            }
        }

        public long CopyFoldersStructure(long sourceFolderId, string newFolderName, bool forceFolderMarging = false)
        {
            TryInitialize(vaultConfig.ServerName, vaultConfig.VaultName, vaultConfig.UserName, vaultConfig.Password, vaultConfig.CasesRootFolderId);
            var sourceFolder = service.GetFolderById(sourceFolderId);

            IFolderAdapter newRootFolder = null;

            if (forceFolderMarging)
            {
                newRootFolder = AddFolderIfDoesntExist(newFolderName, rootFolderId, sourceFolder.IsLib);
            }
            else
            {
                newRootFolder = service.AddFolder(newFolderName, rootFolderId, sourceFolder.IsLib);
            }

            CopyFoldersRecusrivly(newRootFolder.Id, sourceFolderId, forceFolderMarging);
            return newRootFolder.Id;
        }

        private IFolderAdapter AddFolderIfDoesntExist(string newFolderName, long rootFolderId, bool sourceFolderIsLib)
        {
            var rootFolder = service.GetFolderById(rootFolderId);
            IFolderAdapter newRootFolder;

            IFolderAdapter[] childFolders = service.GetFoldersByParentId(rootFolderId, false);

            if (childFolders == null ||
                (newRootFolder = childFolders.SingleOrDefault(folder => folder != null && String.Equals(folder.Name, newFolderName))) == null)
            {
                newRootFolder = service.AddFolder(newFolderName, rootFolderId, sourceFolderIsLib);
            }

            return newRootFolder;
        }


        private void CopyFoldersRecusrivly(long rootFolderId, long sourceFolderId, bool forceFolderMarging)
        {

            var childFolders = service.GetFoldersByParentId(sourceFolderId, false);
            IFolderAdapter newParentFolder;

            if (childFolders == null)
            {
                return;
            }

            foreach (var childFolder in childFolders)
            {
                if (forceFolderMarging)
                {
                    newParentFolder = AddFolderIfDoesntExist(childFolder.Name, rootFolderId, childFolder.IsLib);
                }
                else
                {
                    newParentFolder = service.AddFolder(childFolder.Name, rootFolderId, childFolder.IsLib);
                }

                CopyFoldersRecusrivly(newParentFolder.Id, childFolder.Id, forceFolderMarging);
            }

        }

        public IConnectionAdapter LogIn(string server, string vaultName, string user, string password)
        {
            //WebServiceManager.AllowUI = false;
            //LicenseManager.Instance.SetActive();

            connectionManager = new ConnectionManagerAdapter();
            //var results = connectionManager.LogIn(@"192.168.0.12", "MAT", @"MAT\m.labuda", "!!M23qwerasd");
            var results = connectionManager.LogIn(server, vaultName, user, password);
            //var cos = VDF.Vault.Library.ConnectionManager.GetExistingConnection(@"192.168.0.12", "MAT", @"MAT\m.labuda", "!!M23qwerasd", AuthenticationFlags.WindowsAuthenticationWithCredentials);
            if (!results.Success)
            {
                string message = String.Join("|" ,results.ErrorMessages.Select(x => $"{x.Key} : {x.Value}"));
                throw new ApplicationException($"Could not log into Vault {results.ErrorMessages}. Details: {message}");

            }

            return results.Connection;
        }

        private void LogOut()
        {
            Initilized = false;
            if (connection != null)
            {
                connectionManager.LogOut();
                service.Dispose();
                connection.Dispose();
            }
            service = null;
            connection = null;

        }

        #region IDisposable Support


        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    LogOut();
                }

                disposedValue = true;
            }
        }

        ~VaultDocumentsRepository()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }

        public IVaultBom GetBomByItemId(long itemId)
        {
            TryInitialize(vaultConfig.ServerName, vaultConfig.VaultName, vaultConfig.UserName, vaultConfig.Password, vaultConfig.CasesRootFolderId);

            var item = new VaultBom(itemId, connection.BomService);
            item.InitBom();

            return item;
        }

        #endregion

    }
}