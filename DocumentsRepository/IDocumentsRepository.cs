﻿using VaultWrapper.VaultRepository.Interfaces;

namespace DocumentsRepository
{
    public interface IDocumentsRepository
    {
        /// <summary>
        /// Returns Id of newly created folder
        /// </summary>
        /// <param name="sourceFolderId"></param>
        /// <param name="newFolderName"></param>
        /// <param name="force"></param>
        /// <returns></returns>
        long CopyFoldersStructure(long sourceFolderId, string newFolderName, bool force = false);
        IFolderAdapter GetFolder(long folderId, bool getChildFoldersRecusivly= false);
        IFolderAdapter GetRootTemplatesFolder();
        IVaultBom GetBomByItemId(long itemId);
    }
}