﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using VDF = Autodesk.DataManagement.Client.Framework;
using Autodesk.DataManagement.Client.Framework.Vault.Services.Connection;

namespace VaultWrapper
{
    public class VaultApi
    {
        private VDF.Vault.Currency.Connections.Connection connection;
        private IFolderManager folderManager;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newFolderName">the new name</param>
        /// <param name="rootFolderId">id of the folder that will contain </param>
        /// <param name="baseFolderId">id ot hte folder that will be copied into rootFolder under newFolderName</param>
        public void CreateFolderStructure(string newFolderName, long rootFolderId, long templateFolderId)
        {
            LogInToVault();
            CopyFoldersStructure(newFolderName, rootFolderId, templateFolderId);
            LogOut();
        }


        private void LogInToVault()
        {
            VDF.Vault.Results.LogInResult results = 
                VDF.Vault.Library.ConnectionManager.LogIn(
                    "localhost", 
                    "TestVault", 
                    "Administrator", 
                    "", 
                    VDF.Vault.Currency.Connections.AuthenticationFlags.Standard, null);

            if (!results.Success)
                return;

           connection = results.Connection;
           folderManager = connection.FolderManager;

           VDF.Vault.Library.ConnectionManager.LogOut(connection);

        }

        private void CopyFoldersStructure(string newFolderName, long rootFolderId, long templateFolderId)
        {
            var rootFolder = folderManager.GetFoldersByIds(new[] { rootFolderId }).Single().Value;
            var templateFolder = folderManager.GetFoldersByIds(new[] { templateFolderId }).Single().Value;
            var newFolderParent = folderManager.CreateFolder(rootFolder, newFolderName, templateFolder.IsLibraryFolder, templateFolder.Category);

            foreach (var childFolder in folderManager.GetChildFolders(templateFolder, false, true))
            {
                CopyFoldersRecursive(newFolderParent.Id, childFolder.Id);
            }

        }

        private void CopyFoldersRecursive(long parentFolderID, long templateFolderID)
        {
            var parentFolder = folderManager.GetFoldersByIds(new[] { parentFolderID }).Single().Value;
            var templateFolder = folderManager.GetFoldersByIds(new[] { templateFolderID }).Single().Value;

            var newParentFolder = folderManager.CreateFolder(parentFolder, templateFolder.EntityName, templateFolder.IsLibraryFolder, templateFolder.Category);

            foreach (var childFolder in folderManager.GetChildFolders(templateFolder, false, true))
            {
                CopyFoldersRecursive(newParentFolder.Id, childFolder.Id);
            }

        }

        private void LogOut()
        {
            if (connection != null)
            {
                VDF.Vault.Library.ConnectionManager.LogOut(connection);
            }
        }
    }
}
